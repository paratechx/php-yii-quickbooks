<?php
/**
 * DataService wrapper class that exposes Employee entity specific functionality
 *
 * @author OnCallSolutions
 * @copyright OnCallSolutions
 * @package  qbo yii-module
 * 
 */
use QuickBooksOnline\API\Data\IPPEmployee;
use QuickBooksOnline\API\Data\IPPEmailAddress;
use QuickBooksOnline\API\Data\IPPTelephoneNumber;

class QboEmployeeDataService extends QboDataService {    
    /**
     * @return string used with query() call to retrieve entities of this type
     */
    protected function getFindAllQueryString() 
    {
        $query = "SELECT * FROM employee WHERE active=true";
        return $query;
    }
    /**
     * @param  IPPEmployee
     * @return int
     */
    public function getEntityId( IPPEmployee $item ) 
    {
        return $item->Id;
    }
    /**
     * @param  IPPEmployee
     * @return string
     */
    public function getDisplayName( IPPEmployee $item ) 
    {
        return $item->DisplayName;
    }
    /** 
     * @param  IPPEmployee
     * @return string|boolean
     */
    public function getEmployeeEmail( IPPEmployee $item ) 
    {
        if ( $item->PrimaryEmailAddr && $item->PrimaryEmailAddr->Address ) {
            return $item->PrimaryEmailAddr->Address;
        } else {
            return false;
        }
    }
    protected function mapLocalFieldsToRemoteEntity( User $userRecord, $requiredParameters = array() ) {
        $params = array();
        if ( $userRecord->firstname ) {
            // for testing add randomized string
            // $params["GivenName"] = $userRecord->firstname . rand();
            $params["GivenName"] = $userRecord->firstname;
        }
        if ( $userRecord->lastname ) {
            $params["FamilyName"] = $userRecord->lastname ;
        }                            
        if ( $userRecord->cell ) {
            // see IPPTelephoneNumber for field details
            // TODO -- parse it more specifc. break it into area code etc
            $params["Mobile"] = new IPPTelephoneNumber( array( "FreeFormNumber" => $userRecord->cell ));
        }
        if ( $userRecord->company && $userRecord->company->name ) {
            $params["CompanyName"] = $userRecord->company->name;
        }
        if ( $userRecord->email ) {
            // see IPPEmailAddress for field details
            $params["PrimaryEmailAddr"]  = new IPPEmailAddress( array( "Address" => $userRecord->email ) );
        }
        
        // turn the activeRecord object into an IPPIntuitEntity        
        return new IPPEmployee( $params );
    }
    /**
     * just a 'dumb' mapper function -- all field integrity validation should happen in the controller
     * -- makes error processing easier
     * 
     * @param  integer $remote_entity_id the id that identifies the quickbook entity on their server
     * @param  User $entityRecord Yii model 
     * @return User same model as passed, just with some fields added
     */

    // load the entity matching the id, and assign selected values to the passed in user object reference
    // TODO: split the logic of complex fields into seperate functions for easier reusability
    protected function mapRemoteEntityToLocalRecord( $entity_id, User $entityRecord ) {
        $entity = $this->Query("SELECT * FROM Employee WHERE id='" . $entity_id . "'");

        // sadly this entity does not tell us much if the id is invalid/deleted. 
        if ( ! is_null( $entity ) && count( $entity ) == 1 ) {
            $entity = array_shift( $entity );
            $attributes = array();
            
            if ( $entity->GivenName ) {
                $attributes["firstname"] = $entity->GivenName;
            }
            if ( $entity->FamilyName ) {
                $attributes["lastname"] = $entity->FamilyName;
            }
            // this phone parsing can be quite variable pending on the quickbook setup
            // TODO make it more robust - handle various types of fields in the IPPTelNum object
            if ( $entity->PrimaryPhone ) {
                $phoneNumber = $entity->PrimaryPhone;
                if ( $phoneNumber->FreeFormNumber ) {
                    $attributes["cell"] = $phoneNumber->FreeFormNumber;
                }
            }
            if ( $entity->PrimaryEmailAddr ) {
                $emailAddress = $entity->PrimaryEmailAddr;
                if ( $emailAddress->Address ) {
                    $attributes["email"] = $emailAddress->Address;
                }
            }
            // let the model deal with the assignments
            $entityRecord->attributes = $attributes;
            // leave it up to the controller to validate the fields!
            // // must check this! cannot create a user without an email
            // if ( ! $userRecord->email ) {
            //     $userRecord->addError("email", "Mapping not created. The quickbooks employee entity MUST have a valid email field.");
            // }
        }
        // return the populated object to the caller, let them deal with saving data and field validation
        return $entityRecord;
    }
}