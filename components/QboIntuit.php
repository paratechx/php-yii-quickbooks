<?php
/**
 * Intuit Helper class
 *
 * A collection of helper function to reduce the clutter between Yii, SDK and online API communications
 * Implements primarily Oauth1.0a, but is facaded to integrate Oauth2.0 in the future
 *
 * Quickbooks essentially runs 2 services
 * PLATFORM SERVICE - anything to authenticate users
 * DATA SERVICE - anything to get authenticated users data
 *
 * Generic steps for interacting with the PLATFORM SERVICE
 * 1. create a data service from the available configuration
 * 2. get the serviceContext = $dataService->getServiceContext();
 * 3. Create a $platformService = new PlatformService($serviceContext);
 * 4. Call Reconnect if you need to refresh your OAuth 1.0a tokens
 * 5. close the connection $result = $platformService->Disconnect();
 *    QBO will always return 200 on status code, so look into the error msg
 *
 * Generic steps for connecting to DATA SERVICE:
 * 1. create a data service from the config array
 * 2. use dataService functions to query the data
 *
 * There are two types of status' that controllers should check:
 * isAlive() - can the target api be reached (ie is there an internet connection )
 * 
 * isAuthenticated() - if the connection is alive, are the stored credentials authorized 
 * 
 * @author OnCallSolutions
 * @copyright OnCallSolutions
 * @package  qbo yii-module
 *
 */
use QuickBooksOnline\API\Core\CoreConstants;
use QuickBooksOnline\API\Core\ServiceContext;

class QboIntuit extends CApplicationComponent {

    // OAUTH1 specific
	private $_app_token 		= false;
	private $_consumer_key		= false;
	private $_consumer_secret	= false;

    private $_oauth             = CoreConstants::OAUTH1;

    private $_credentials               = false; // use these to initiate authorized connections
    private $_verifiedCredentials       = false;

    private $_dataService                   = false; 
    private $_serviceContext                = false;
    private $_companyInfo       = null; // IPPCompanyInfo object when connected


    public $connection_status      = QboConnection::CONNECTION_STATUS; 
	public $authentication_status  = QboConnection::OAUTH_STATUS;

	public $sandbox_mode			= true;
    public $lastError               = false;

    public function init()
    {
        $this->connection_status = QboConnection::checkInternetConnection();
        return parent::init();
    }
    /**
     *  @TODO -- if this gets called once per page load, does it not become expensive??
     * 
     * @return boolean indicating if the api connection can be established
     */
    public function connect() 
    {
        if ( ! $this->_verifiedCredentials ) {
            throw new CHttpException(501,'Not Implemented: Quickbooks-App credentials not configured correctly on your server.');
        }
        // load the object that contains the token and other details - QboToken
        $authorizationToken = $this->_getAuthorizationToken();

        if ( $authorizationToken ) {
            $this->_credentials = $authorizationToken;

            if ( $authorizationToken->isExpired( QboConnection::tokenLife() ) ) {
                $this->authentication_status = QboConnection::OAUTH_EXPIRED;
    
                if ( $this->isAlive() ) {
                    if ( $this->reconnect() ) {
                        $this->authentication_status = QboConnection::OAUTH_ALIVE; 
                        return true;               
                    } else {
                        throw new CHttpException(500,'Reconnecting failed. Unable to use tokens to refresh connection.');       
                    }
                } else {
                    return false;
                }
            }
            // at this point our credentials appear legit, but unless tried we never know
            $this->authentication_status = QboConnection::OAUTH_VALID;
            // to establish legitimacy, attempt to pull the company data
            if ( $this->isAlive() ) {
                // one last check to ensure the credentials are actually valid
                if( $this->confirmConnectionCredentials() ) {
                    $this->authentication_status = QboConnection::OAUTH_ALIVE;
                    return true;
                } else {
                    $this->lastError = "Error! Unable to connect with the current credentials.";
                }
            }
        } else {
            $this->authentication_status = QboConnection::OAUTH_DISCONNECTED;
            $this->lastError = "No connection to quickbooks app established.";
        }
        return false;
    }
    /**
     * check the QboIntuit->lastError for a string if the response is false
     * 
     * @return boolean indicating if the user was able to connect to the intuit-app
     */
    public function reconnect() 
    {
        if ( ! $this->isAlive() ) {
            return false;
        }
        if ( $this->_oauth == CoreConstants::OAUTH1 ) {
            //Create a platform Service
            $platformService = new QboPlatformService( $this->getServiceContext() );

            //to refresh your OAuth 1.0a tokens must call it this way
            $responseXml = $platformService->Reconnect();

            if ( $platformService->validateXmlResponse( $responseXml ) ) {
                if ( $platformService->hasXmlTokens( $responseXml ) ) {
                    $message =  "Reconnect successful! New oAuth tokens issued.";

                    // store reconnect tokens!
                    $token = (string) $responseXml->OAuthToken;
                    $secret = (string) $responseXml->OAuthTokenSecret;
                    return $this->_credentials->updateTokenFields( $token, $secret );
                } else {
                    $message = "Error! New oAuth tokens not found in responseXml.";
                }
            }
            // if we get to here then reconnect failed, so set some clues!
            $this->lastError = $platformService->lastError;
        } elseif ( $this->_oauth == CoreConstants::OAUTH2 ) {
            // implement in future if intuit forces update

            // from docs:
            // $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
            // $accessToken = $OAuth2LoginHelper->refreshToken();
            // $error = $OAuth2LoginHelper->getLastError();
            // if ($error != null) {
            //     echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
            //     echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            //     echo "The Response message is: " . $error->getResponseBody() . "\n";
            //     return;
            // }
            // $dataService->updateOAuth2Token($accessToken);
            $this->lastError = "Oauth2 reconnect not implemented!";
        }
        return false;
    }
    /**
     * simple disconnect logic
     * 
     * @return boolean indicating if the app is disconnected from the user credentials
     */
    public function disconnect() 
    {
        if ( $this->_oauth == CoreConstants::OAUTH1 ) {
            //Create a platform Service
            $platformService = new QboPlatformService( $this->getServiceContext() );
            
            // TODO - analyze the response
            // QBO will always return 200 on status code, so look into the error msg
            
            $responseXml = $platformService->Disconnect();

            if ( $platformService->validateXmlResponse( $responseXml ) ) {
                // clean up the local database storage
                return $this->_removeAuthorizationToken();    
            } else {
                $this->lastError = $platformService->lastError;
            }            
        } elseif ( $this->_oauth == CoreConstants::OAUTH2 ) {
            // implement in future if intuit forces update
            $this->lastError = "Oauth2 not implemented for the disconnect action";
        }
        return false;
    }

    /**
     * a middleman validation function because Intuit issues different credentials in oauth1 than oauth2
     * 
     * @param boolean that acknowledges if the credentials were of the right type
     */
    public function setCredentials( $credentials ) {
        if ( $this->_oauth == CoreConstants::OAUTH1 && 
             $this->isValidOauth1Credentials( $credentials ) ) {

                // TODO wrap in something more generic for easier reuse
                $this->_app_token       = $credentials['app_token'];
                $this->_consumer_key    = $credentials['consumer_key'];
                $this->_consumer_secret = $credentials['consumer_secret'];

                $this->_verifiedCredentials = true;

            } elseif ( $this->_oauth == CoreConstants::OAUTH2 ) {
                // TODO - future implementation
            }
        return $this->_verifiedCredentials;
    }
    /** 
     * cleaned out of regular logic to simplify Oauth2 transition
     * 
     * @param  array with named keys that contains credentials obtained from the app-portal
     * 
     * @return boolean confirming the validity of indexes
     */
    public function isValidOauth1Credentials( $credentials ) 
    {
        if ( array_key_exists( "app_token", $credentials ) &&
             array_key_exists( "consumer_key", $credentials ) &&
             array_key_exists( "consumer_secret", $credentials ) ) {
            return true;
        }
        return false;
    }
    /**
     * as suggested by Intuit-SDK comments, query the company details to ensure the connection is legit
     * 
     * @param  boolean forcing the connection test
     * @return boolean indicating if our connection is alive
     */
    private function confirmConnectionCredentials( $forceTest = false ) 
    {
        // DEBUG OPTION:
        // $this->authentication_status = QboConnection::OAUTH_ALIVE;
        // return true;

        // test when forced, or when global flag says not connected
        if ( $forceTest || $this->authentication_status != QboConnection::OAUTH_ALIVE ) {
            if ( $this->getCompanyInfo() != null ) {
                $this->authentication_status = QboConnection::OAUTH_ALIVE;
            } 
        }
        return $this->isAlive(); 
    }
    /**
     * @return boolean telling caller if api can be contacted
     */
    public function isAlive() 
    {
        return ( $this->connection_status == QboConnection::CONNECTION_ALIVE );
    }
    /**
     * @return boolean telling caller if connection is alive (valid credentials that are not expired)
     */
    public function isAuthenticated() 
    {
        return ( $this->authentication_status >= QboConnection::OAUTH_VALID );
    }
    /** 
     * @return QboToken record of the authorization token for this company
     */
    private function _getAuthorizationToken() {
        $user = Yii::app()->user;

        if ( isset( $user->companyId ) ) {
            $company_id = $user->companyId;
            $app_token = $this->_app_token;
            $token = QboToken::model()->findCompany( $company_id, $app_token );

            if ( ! is_null( $token ) ) {
                // dev tool to test different tokens
                $token = $this->tokenOverride( $token );
                return $token;
            }
        } else {
            throw new CHttpException(501,'Could not located a company assigned to this user. Quickbooks module requires a company id for each connection.');
        }
        return false;
    }
    /**
     * @param  QboToken of an actual record
     * @return QboToken with modified token values
     */
    private function tokenOverride( $tokenRecord ) {
        $token = Yii::app()->request->getQuery("temptoken");
        $tokenSecret = Yii::app()->request->getQuery("tempsecret");
        if ( ! is_null( $token ) && ! is_null ( $tokenSecret ) ) {
            $tokenRecord->setTokenFields($token, $tokenSecret );    
        }
        return $tokenRecord;
    }

    private function _getConfigDetails() {
        // TODO this is dangerous! we need to limit the exposure from unset token objects...
        if ( $this->_credentials == null ) {
            throw new CHttpException( 500, "QboIntuit configuration error. Must set credentials first." );            
        }
        // Get App Config
        $realmId = $this->_credentials->getRealmId();

        if (!$realmId)
            throw new CHttpException( 500, "Invalid RealmId" );

        $config =   array( "auth_mode"          => $this->_oauth,
                           "consumerKey"        => $this->_consumer_key,
                           "consumerSecret"     => $this->_consumer_secret,
                           "accessTokenKey"     => $this->_credentials->getToken(),
                           "accessTokenSecret"  => $this->_credentials->getTokenSecret(), 
                           "QBORealmID"         => $this->_credentials->getRealmId(),
                           "baseUrl"            => QboConnection::getApiBaseUrl( $this->sandbox_mode ) 
                       ); 
        return $config;
    }

    private function _initilizeServiceContext() {
       $serviceContext = ServiceContext::configureFromPassedArray( $this->_getConfigDetails() );

        if (!$serviceContext)
            throw new CHttpException( 500, "Unable to setup serviceContext object." );
        else 
            $this->_serviceContext = $serviceContext;

        return $this->_serviceContext;
    }

    public function getServiceContext() {
        if ( ! $this->_serviceContext ) {
            return $this->_initilizeServiceContext();
        } 
        return $this->_serviceContext;
    }
    /**
     * @return boolean 
     */
    private function _initializeDataService() {
        $dataService = new QboDataService( $this->getServiceContext() );

        // TODO -- is there a more graceful way to fail?
        if ( !$dataService ) {
            throw new CHttpException(500,'Cannot initialize the Intuit DataService object.');
        } else {
            return $this->_dataService = $dataService;
        }   
    }
    /**
     * @return QboDataService child of the SDK DataService
     */
    public function getDataService() {
        if ( ! $this->_dataService ) {
            return $this->_initializeDataService();
        } 
        return $this->_dataService;
    }
    /**
     * TODO -- not practical as it only works for the DataService object
     * a helper to decipher API responses and feed them into Yiis flash system
     * 
     * @param  [type]
     * @param  boolean
     * @return [type]
     */
    public function printErrorDetails($error, $yiiFlash = true) {

        if ( $this->lastError != null ) {
            $error = $this->lastError;
            if ( $yiiFlash ) {
                $message =  "Error Details: " . 
                            "[HttpStatusCode: " . $error->getHttpStatusCode() . "], " .
                            "[ServerMessage: " . $error->getOAuthHelperError() . "], ";
                
                Yii::app()->user->setFlash('error', "Unable to connect - " . $message);

            } else {
                echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
                echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                echo "The Response message is: " . $error->getResponseBody() . "\n";
                echo "The Intuit Helper message is: IntuitErrorType:{" . $error->getIntuitErrorType() . "} IntuitErrorCode:{" . $error->getIntuitErrorCode() . "} IntuitErrorMessage:{" . $error->getIntuitErrorMessage() . "} IntuitErrorDetail:{" . $error->getIntuitErrorDetail() . "}";
            }
            return true;
        }
        return false;
    }
    /**
     * using this as a helper clutch to determine if our connection is legit
     * 
     * @return CompanyInfoEntity object that proves we can connect
     */
    public function getCompanyInfo()
    {
        $dataService = $this->getDataService();

        $companyInfo = $dataService->getCompanyInfo();
        $error = $dataService->getLastError();

        if ($error != null) {
            $this->printErrorDetails( $error, true );
        } else {
            $this->_companyInfo = $companyInfo;
        }
        return $companyInfo;
    }

	// initiates the process to receive the authorization token through the user login to quickbooks
	public function authorizeConnection() {
			// tie into existing oauth library
			$quickbooks_oauth_url 		= Yii::app()->createAbsoluteUrl( QboConnection::QBO_SLUG_OAUTH, array(), QboConnection::QBO_SCHEME);

			$server = new QboOauth1Client(array(
			    'identifier'   => $this->_consumer_key,
			    'secret'       => $this->_consumer_secret,
			    'callback_uri' => $quickbooks_oauth_url
			));

			// Retrieve temporary credentials
			$temporaryCredentials = $server->getTemporaryCredentials();

			// Store credentials in the session, we'll need them later
			$_SESSION['temporary_credentials'] = serialize($temporaryCredentials);

			// Second part of OAuth 1.0 authentication is to redirect the
			// resource owner to the login screen on the server.
			$server->authorize($temporaryCredentials);

			return true;
	}
	// usually invoked by the controller after api server tries to handshake with consumer
	public function handleAuthorizationRespone() {
        // tie into existing oauth library
		$quickbooks_oauth_url = Yii::app()->createAbsoluteUrl( QboConnection::QBO_SLUG_OAUTH, array(), QboConnection::QBO_SCHEME);

		$server = new QboOauth1Client (array(
		    'identifier'   => $this->_consumer_key,
		    'secret'       => $this->_consumer_secret,
		    'callback_uri' => $quickbooks_oauth_url
		));

		if (   isset($_GET['oauth_token']) && isset($_GET['oauth_verifier']) 
            && isset($_GET['realmId']) && isset($_GET['dataSource']) ) {
		    // Retrieve the temporary credentials we saved before
		    $temporaryCredentials = unserialize($_SESSION['temporary_credentials']);

		    $realm_id   = Yii::app()->request->getQuery("realmId", false);
            $source     = Yii::app()->request->getQuery("dataSource", false);

		    // We will now retrieve token credentials from the server
		    $tokenCredentials = $server->getTokenCredentials($temporaryCredentials, $_GET['oauth_token'], $_GET['oauth_verifier']);

            // can the token credentials failr?!?!
            // lets assume not for now
            // CredentialsException will be thrown if the post is not successful, but could Intuit return a "valid" negative response? 
            $response = $this->_handleTokenStorage( $this->_app_token, $this->_consumer_key, $tokenCredentials->getIdentifier(), $tokenCredentials->getSecret(), $realm_id, $source );

            if ( $response ) {  // properly stored
                return true;
            }
		}
        return false;

	}
    private function _handleTokenStorage($app_token, $consumer_key, $access_token, $access_secret, $realm_id, $source) {
        $user = Yii::app()->user;

        $token = QboToken::model()->findCompany( $user->companyId, $app_token );

        if ( is_null( $token ) ) {
            $token = new QboToken;
            $token->assignOwner( $user->id, $user->companyId );
        }
        $token->setTokenData( $app_token, $access_token, $access_secret, $realm_id, $source );

        return $token->save();
    }
    private function _removeAuthorizationToken() {
        $user = Yii::app()->user;
        $app_token = $this->_app_token;
        $token = QboToken::model()->findCompany( $user->companyId, $app_token );

        if ( is_null( $token ) ) {
            return false;
        }
        return $token->delete();
    }
	public function getDisconnectUrl() {

		$url = Yii::app()->createAbsoluteUrl( QboConnection::QBO_SLUG_OAUTH_DISCONNECT, array(), QboConnection::QBO_SCHEME);
		return $url;
	}
    public function getReconnectUrl() 
    {
        $url = Yii::app()->createAbsoluteUrl( QboConnection::QBO_SLUG_OAUTH_RECONNECT, array(), QboConnection::QBO_SCHEME);
        return $url;
    }

	public static function getButtonScriptUrl() {
		return QboConnection::INTUIT_APIWIDGET_URL;
	}
	// script to initialize the Widget script
	public static function getIppSetupScript() {
		$quickbooks_menu_url 	= Yii::app()->createAbsoluteUrl( QboConnection::QBO_SLUG_OAUTH_MENU, array(), QboConnection::QBO_SCHEME);
		$quickbooks_grant_url	= Yii::app()->createAbsoluteUrl( QboConnection::QBO_SLUG_OAUTH_GRANT, array(), QboConnection::QBO_SCHEME);
		return "
			intuit.ipp.anywhere.setup({
				menuProxy: '$quickbooks_menu_url',
				grantUrl: '$quickbooks_grant_url'
			});
		";
	}
    /**
     * helper function to achieve same window oauth connection workflow
     *
     * Note: relies on https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js to have the correct functions!
     * 
     * @return string show javascript that gets injected via Yiis registerScript function
     */
    public static function getDirectConnectScript() {
        return "
            if ( intuit ) {
                var redirectUrl = intuit.ipp.anywhere.controller.getOAuthUri();
                console.log( 'Redirecting to intuit servers: ' + redirectUrl );
                if ( redirectUrl ) {
                    window.location = redirectUrl;
                }
            }            
        ";   
    }

    /**
     * a fragile function to get the bluedot menu
     * requires an override via QboSyncRestHandler to get it working 
     * 
     * @return string html to render the blue dot menu
     */
    public function getAppMenu() 
    {
        if ( ! isset( $_SESSION["quickbooks_cached_app_menu"] ) ) {
            // Prep Platform Services
            $platformService = new QboPlatformService( $this->getServiceContext() );

            // Get App Menu HTML
            $html = $platformService->GetAppMenu();
            // TODO - validate the response/verify the htmlResponse codes
            // store it so we don't call it everytime
            $_SESSION["quickbooks_cached_app_menu"] = $html;        
        }
        return $_SESSION["quickbooks_cached_app_menu"];
    }
    public function getUserDetails() 
    {
        if ( $this->serviceUnavailable() ) {
            return "Api Service Offline. No User details available.";
        }
        $platformService = new QboPlatformService( $this->getServiceContext() );

        //to refresh your OAuth 1.0a tokens must call it this way
        $responseXml = $platformService->currentUser();

        if ( $platformService->validateXmlResponse( $responseXml ) ) {
            if ( $platformService->hasXmlUserData( $responseXml ) ) {
                return (string) $responseXml->User->ScreenName;
            } else {
                return "Bad Xml Response. No user details found.";
            }
        } else {
            return "Unable to validate the Xml Response of user details";
        }
    }
    /**
     * give us a literal string for the connection status - used mainly for debugging on the /qbo/connection view
     * 
     * @return string containing readable version of the connection status
     */
    public function getAuthenticationStatusString() 
    {
        $statusName = "Disconnected";
        switch( $this->authentication_status ) {
            case QboConnection::OAUTH_ALIVE:
                $statusName = "Alive";
                break;
            case QboConnection::OAUTH_EXPIRED:
                $statusName = "Expired";
                break;
            case QboConnection::OAUTH_VALID:
                $statusName = "Valid";
                break;                
        }
        return $statusName;
    }
    /**
     * @return string indicating if the connection to the API can be made
     */
    public function getStatusString() 
    {
        $statusName = "Unable to reach the API";
        switch( $this->connection_status ) {
            case QboConnection::CONNECTION_ALIVE:
                $statusName = "Quickbooks API available";
                break;
        }
        return $statusName;
    }

    /**
     * @return string url based on sandbox variable
     */
    public function getApiBaseUrl() 
    {
        return QboConnection::getApiBaseUrl( $this->sandbox_mode );
    }
    /**
     * @return string of token
     */
    public function getAppToken() 
    {
        return $this->_app_token;
    }
    /**
     * @return string of token
     */
    public function getConsumerKey() 
    {
        return $this->_consumer_key;
    }
    /**
     * @return string of token
     */
    public function getConsumerSecret() 
    {
        return $this->_consumer_secret;
    }
    /**
     * @return string of token
     */
    public function getAccessToken() 
    {
        if ( $this->_credentials instanceOf QboToken )
            return $this->_credentials->getToken();
        else 
            return "Not set.";
    }
    /**
     * @return string of token
     */
    public function getAccessTokenSecret() 
    {
        if ( $this->_credentials instanceOf QboToken )
            return $this->_credentials->getTokenSecret();
        else 
            return "Not set.";
    }  
    /**
     * @return string of token
     */
    public function getRealmId() 
    {
        if ( $this->_credentials instanceOf QboToken )
            return $this->_credentials->getRealmId();
        else 
            return "Not set.";        
    }        
    /**
     * @return integer timestamp of when the access token will expire - don't forget to format to make it readable!
     */
    public function getTokenDeadlineTimestamp() 
    {
        // load the object that contains the token and other details - QboToken
        $authorizationToken = $this->_getAuthorizationToken();

        if ( $authorizationToken ) {
            $expiryDate = $authorizationToken->getExpiryDate( QboConnection::tokenLife() );
            return $expiryDate;
        } else {
            // @TODO -- throw an error instead!
            return time();
        }        
    }
    /**
     * @return integer timestamp of when the access token will expire - don't forget to format to make it readable!
     */
    public function getTokenIssueTimestamp() 
    {
        // load the object that contains the token and other details - QboToken
        $authorizationToken = $this->_getAuthorizationToken();

        if ( $authorizationToken ) {
            $requestStamp = $authorizationToken->getRequestTimestamp();
            return $requestStamp;
        } else {
            // @TODO -- throw an error instead!
            return time();
        }        
    }
    /**
     * @return string of the quickbooks company name that user connected to
     */
    public function getCompanyName() 
    {
        if ( $this->serviceUnavailable() ) {
            return "Api Service Offline. No Company details available.";
        }        
        if ( $this->authentication_status == QboConnection::OAUTH_ALIVE ) {
            if ( ! is_null( $this->_companyInfo ) ) {
                return $this->_companyInfo->CompanyName;
            }
        } else {
            return "Unable to read company name.";
        }
    }
    /**
     * @return boolean evaluation of the QboIntuit->connection_status 
     */
    protected function serviceUnavailable() 
    {
        return $this->connection_status != QboConnection::CONNECTION_ALIVE;
    }
}