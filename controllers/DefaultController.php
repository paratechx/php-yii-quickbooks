<?php

class DefaultController extends Controller
{
    public $quickbooks_is_available     = false;    // can we ping the api and connect to it?
	public $quickbooks_is_authenticated = false;    // do we have authorized tokens that provide us with data?

	public $disconnect_url 			= false;
	public $authentication_redirect = true;

	private $_accessControlRules;


	/**
	 * helper component to be used by all controllers to verify existing credentials
	 * 
	 * @return the Intuit component object that glues the remote access to this user
	 */
	public function getIntuitObject() 
	{
		$Intuit = $this->module->getIntuit();
		if ( $this->_enforceApiConnection( $Intuit ) );
			return $Intuit;
	}
	/**
	 * keep it seperate for easier portability to other controllers. 
	 * 
	 * @return boolean for success of init
	 */
	public function initIntuit() {
		$Intuit = $this->getIntuitObject();

		// help with display of floating blue button and connect buttons
		$this->appendWidgetScripts();		

        $this->quickbooks_is_available      = $Intuit->isAlive();
        if ( ! $this->quickbooks_is_available ) {
            Yii::app()->user->setFlash('error', "Unable to contact the quickbooks app. Please try again later.");
        }
        $this->quickbooks_is_authenticated	= $Intuit->isAuthenticated();

        $this->disconnect_url 			= $Intuit->getDisconnectUrl();

        return true;
	}
	/**
	 * @return render a basic index view
	 */
	public function actionIndex()
	{
		// since this is also our authentication initiator, we need to disable the redirect
		$this->authentication_redirect = false;

		$this->initIntuit();

		$this->render('index');
	}
    
    public function actionError()
    {
        echo "error";

        if($error=Yii::app()->errorHandler->error)
        {
                if( Yii::app()->request->isAjaxRequest )
                        echo $error['message'];
                else {
                    $this->layout='main';
                    $this->render('error', $error);
                }
                    
        }
            
    }

	/**
	 * @return array action filters
	 */
	public function filters() 
	{
		return array(
		  'accessControl', // perform access control
		);
	}
    public function accessRules()
    {
        return array(
            array('deny',
                'users'=>array('?'),
            ),
            array('allow',
                'roles'=>array('PayrollClerk', 'admin'),
            ),
        );
    }       
	/**
	 * @param  QboIntuit object
	 * @return boolean or redirect with set error message
	 */
	protected function _enforceApiConnection( $Intuit ) 
	{
		if ( ! $Intuit->isAuthenticated() ) {
			Yii::app()->user->setFlash('error', "No valid credentials linked to quickbooks account.");

			// send them to reauthenticate the connection
			if ( $this->authentication_redirect )
				$this->redirect( array("/qbo") );
		}
		return true;
	}	
	/**
	 *	an overwrite to append the widget scripts 
     *  TODO - needed in the past to display the Intuit button - still a requirement??
	 * 
	 * @param  [type]
	 * @param  [type]
	 * @param  boolean
	 * @return [type]
	 * @deprecated maybe??
	 */
	public function render($view,$data=null,$return=false) 
	{
        if ( $this->quickbooks_is_available ) { // otherwise the various js calls will throw errors
    		$this->appendWidgetScripts();
        }
		return parent::render( $view, $data, $return );
	}
	/**
	 * @return none - simply queues the button scripts to the yii rendering engine
	 */
	public function appendWidgetScripts() {
		Yii::app()->clientScript->registerScriptFile( QboIntuit::getButtonScriptUrl() );
		// snippet to allow the button insertions for quicbooks connector
		Yii::app()->clientScript->registerScript('quickbooks_helper', QboIntuit::getIppSetupScript() );
	}
    /**
     * used mostly in the OauthController to hide any of the log output that would mess with a json data display
     * anticipating the use in other controllers, so keeping it here for now
     * 
     * @return none
     */
    public function disableCwebLogOutput() 
    {
        // TODO make this more robust. see solution at 
        // http://www.yiiframework.com/forum/index.php/topic/15110-disabling-cweblogroute-inside-controller-action/
        //
        // this only works if the log route is named
        Yii::app()->log->routes['cweb']->enabled=false;
    }
    /**
     * @param  array with data identifiers
     * @return array of key=>value pairs of allowed url actions 
     */
    public function getActionLinks($data) {
        return array();
    }
    protected function beforeAction($action) {
        return true;
    }
    /**
     * a wrapper to standardize the json outputs and deal with the jquery scripts
     * 
     * @param string $view name of the view to be rendered. See {@link getViewFile} for details
     * about how the view script is resolved.
     * @param array $data data to be extracted into PHP variables and made available to the view script
     * @param boolean $return whether the rendering result should be returned instead of being displayed to end users
     * @param boolean $processOutput whether the rendering result should be postprocessed using {@link processOutput}.
     * @param string $status describes what to do with the json output
     * @return string the rendering result. Null if the rendering result is not required.
     */
    protected function renderJSON($view,$data=null,$return=true,$processOutput=false ) {
        // Create default array for scripts which should be disabled
        $disableScripts = array(
            'jquery.js',
            'jquery.min.js',
            'jquery-ui.min.js'
        );
        // Disable scripts
        foreach( $disableScripts as $script )
            Yii::app()->clientScript->scriptMap[$script] = false;

        // catch missing data elements
        if ( $view == "/partial_views/_ajax_message" &&
             ! array_key_exists("message", $data) ) {
            $data["message"] = "";
        }
        if ( ! array_key_exists("status", $data ) ) {
            $data["status"] = "render";
        }

        header("Content-type: application/json;charset=utf-8");
        // Output JSON encoded content
        echo CJSON::encode( array(
            'status' => $data["status"],
            'content' => $this->renderPartial( $view ,$data, $return ,$processOutput))
        );
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute) {
                    $route->enabled = false;
            }
        }
        // Stop script execution
        Yii::app()->end();    
      }
    /**
     * Render JSON data for ajax
     * If data is_bool then render json where key='result' and value=data
     * @param mixed $data
     */
    // public function renderJSON($data)
    // {
       //  if (is_bool($data))  {
    //         $data = array('result' => $data);
       //  }
       //  header("Content-type: application/json;charset=utf-8");
       //  echo CJSON::encode($data);
       //  foreach (Yii::app()->log->routes as $route)
       //  {
    //         if ($route instanceof CWebLogRoute)
    //         {
    //                 $route->enabled = false;
    //         }
       //  }
       //  Yii::app()->end();
    // }
    public function getDefaultPageSize()
    {
        //return Yii::app()->params['defaultPageSize']
        return 25;
    }
    public function getPageSizeOptions() 
    {
        // you can config it in main.php under the config dir . Yii::app()->params['pageSizeOptions'],
        // Optional, you can use with the widget default
        return array(1=>1,2=>2,5=>5, 10=>10, 25=>25, 50=>50, 75=>75, 100=>100);
    }
    protected function adjustPageSize() 
    {
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }        
    } 
    /**
     * implementing a more robust way to fetch yiis parameters...
     * see 
     *
     * http://www.yiiframework.com/wiki/126/setting-and-getting-systemwide-static-parameters/
     * 
     * @param  string
     * @param  mixed
     * @return mixed the value or default if not found in the parameter array
     */
    protected function getYiiParameter($name, $default = null)
    {
        if ( isset(Yii::app()->params[$name]) )
            return Yii::app()->params[$name];
        else
            return $default;
    }
}