<?php
/**
 * Simple helper component to provide routes for hawki menu items
 *
 *   USAGE	
 * 		use in the app/views/layouts/loggedinmenu.php file or any other menu layout
 *		call with
 *		Yii::app()->getModule('qbo')->menu->getTopLevelMenu()
 *
 *   ie for hawki around line 144:
 *       array(
 *         "label"=>"Advanced",
 *         //"visible"=>($superuser == true) || (!Yii::app()->user->isGuest && in_array(Yii::app()->user->companyId,array(10259,10005,10329))),
 *         'items' => array(
 *           array("label"=>"Ceridian PowerPay export",  "url"=>array("route"=>"/Ceridian")),
 *         ),
 *       ),
 *       Yii::app()->getModule('qbo')->menu->getTopLevelMenu()
 *
 * @author OnCallSolutions
 * @copyright OnCallSolutions
 * 
 */
class QboMenu extends CApplicationComponent
{
  /**
   * adjust as needed or define additional constraints utilizing the subscription service
   * 
   * @return array that contains a simple menu for the quickbooks module
   */
  public function getTopLevelMenu() 
  {
    // fancy menu
    $menu = array(
          "label"=>"Quickbooks",
          "url"=>array("route"=>"/qbo"),
          'items' => array(
            array("label"=>"Connection Status",  "url"=>array("route"=>"/qbo/connection")),
            array("label"=>"Employees", "url"=>array("route"=>"/qbo/employee/list")),
            array("label"=>"Jobs", "url"=>array("route"=>"/qbo/customer/list")),
            array("label"=>"Services", "url"=>array("route"=>"/qbo/item/list")),
            array("label"=>"Export History", "url"=>array("route"=>"/qbo/timeactivity/list")),
            array("label"=>"Payroll Export", "url"=>array("route"=>"/qbo/timeactivity/export")),
            )
          );
    return $menu;
  }
}