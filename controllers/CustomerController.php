<?php
/**
 * handles all logic related to the mapping between
 * QUICKBOOKS - customer.jobs and HAWKI - project entities
 *
 * 
 */
class CustomerController extends DefaultEntityController
{
    protected $model;
    protected $local_entity_name        = "project";
    protected $remote_entity_name       = "customer";

    protected function processLocalEntityCreation( $newProject ) 
    {
        $loggedInUser = Yii::app()->user;
        // TODO validate that the user is logged in? Controller should already have caught it!
        if ( $newProject->name ) {
            $newProject->company_id      = $loggedInUser->companyId;
            $newProject->create_user_id  = $loggedInUser->id;
            $newProject->update_user_id  = $loggedInUser->id;
        } else {
            $newProject->addError("name", "Remote Task must have an valid name field set. " );
        }
        // TODO -- map some of those required fields that might not be set from quickbooks
        // leave it for now
        $newProject->hoursbudget_enable = 0;

        return $newProject;
    }

    protected function actionCreateRequiredRemoteAttributes( $local_entity_id ) {
        $request = Yii::app()->getRequest();
        $model = $this->getLocalEntityModel()->findByPk( $local_entity_id );

        if ( $request->isPostRequest ) {    // analyze the response and provide result to our caller
            $modelData = $request->getPost( get_class($model) );
            $action = strtolower( $request->getPost("postaction") );
            $customer_id = intval( $request->getPost("customer_id") );

            // this is dirty, but gets the job done for now (id:CUR )
            // NOTE: if a customer does not have the same currency as the default currency, then creating 
            // a new customer-job will cause a currency mismatch, throwing API errors. To avoid this, we
            // encode the currency value with this minor hack-job
            $currency_name = explode(":", $request->getPost("customer_id"));
            $currency_name = strtoupper(($currency_name && count( $currency_name ) ? end( $currency_name ) : false));
            
            // validate
            if ( $action == "create" &&
                 $customer_id > 0 &&
                 strlen( $currency_name ) == 3 ) {
                // pass back to create parent
                return array( "customer_id" => $customer_id, "currency_name" => $currency_name, "rendered" => false );
            } else {
                // TODO -- call addError and tell parent what went wrong
                return array( "rendered" => false );
            }
        } else {  // show the user a selection of required attributes that need to be set before we can proceed
            $dataService = $this->getDataService();
            $topLevelCustomers = $dataService->getTopLevelCustomers();

            if( ! count( $topLevelCustomers ) ) {
                // TODO -- throw errors
            }

            if ( ! is_null( $model ) ) {
                // TODO - figure out the defaultSelector values to make it preselect accounts
                $this->render("create_attributes", array( "model"                   => $model,
                                                          "cancelRoute"             => $this->getListRoute(),
                                                          "topLevelCustomers"       => $topLevelCustomers,
                                                          "defaultCustomer"         => "1",    // the select-function compares to a string of the submitted value
                                                        ));
            } else {
                // throw invalid local entity error
            }
            return array("rendered" => true );
        }
    
    }    
}