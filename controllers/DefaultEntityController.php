<?php

/**
 * generic parent that helps to minimize entity function polution in the defaultController
 * 
 */

class DefaultEntityController extends DefaultController
{
    protected $local_entity_name;
    protected $remote_entity_name;
    protected $model;

    protected $refresh_url;

    public function actionIndex() 
    {
        $this->render('index');
    }
    /** 
     * general list of mapped entities
     * 
     * @return rendered view
     */
    public function actionList() 
    {
        $filter = $this->getModel();

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }
        // attach a list of all available entities to the view
        $this->render('list', array(
            'pageSize'      => Yii::app()->user->getState('pageSize', $this->getYiiParameter("defaultPageSize", 10 )),
            'filter'        => $this->model,
            'dataProvider'  => $this->model->getMapUniverse(),
        ));
    }
    public function getListRoute() 
    {
        return array( "/" . $this->getUniqueId() . "/list" );
    }    
    /**
     *  TODO: make the model access cleaner and universalize the getMappedName function
     * 
     * @param  integer of id that identitfies entity in hawki database
     * @return string None|a link to the hawki view for the referenced entity
     */
    public function getLocalEntityLink( $entity_id ) {
        if ( is_null( $entity_id ) ) {
            return "None";
        }        
        $entity_table_name = $this->model->entityTableName();
        $entity_display_name = $this->model->getMappedName( $entity_id );

        if ( ! $entity_display_name ) { // very likely a mismap (ie local entity has been deleted )
            $label = CHtml::encode( "Mismapped Entity. Click here to fix!" );
            $linkSegements = array($this->id . "/unmap/", "lid" => $entity_id );
            $altLabel = array("title"=>"View $entity_table_name.");

        } else {
            $label = CHtml::encode( $this->model->getMappedName( $entity_id ) );
            $linkSegements = array("/$entity_table_name/view", "id" => $entity_id );
            $altLabel = array("title"=>"View $entity_table_name.");
        }
    
        return CHtml::link( $label, $linkSegements, $altLabel );
    }
    public function getQuickbooksEntityLink( $entity_id, $display_name ) {
        if ( is_null( $entity_id ) ) {
            return "None";
        }
        $entity_table_name = "Quickbooks " . $this->getQuickbooksEntityName();
        $label = CHtml::encode( $display_name );
        $link = QboConnection::getIntuitGuiUrl( $this->getQuickbooksEntityName() );
        $altLabel = array("title"=>"View $entity_table_name.");

        return CHtml::link( $label, $link, $altLabel );
    }

    /**
     * @return string describing the entity name in a readable way
     */
    public function getHawkiEntityName() 
    {
        return $this->local_entity_name;
    }
    /**
     * @return string describing the entity name in a readable way
     */
    public function getQuickbooksEntityName() 
    {
        return $this->remote_entity_name;
    }
    /**
     * @return array with link segments that can be used by CHtml::link to render the refresh url for this entity
     */
    public function getRefreshRoute() 
    {
        return array( $this->remote_entity_name . "/refresh/");        
    }
    /**
     * allowing client to "click to refresh"
     * it updates the local mapping table with any new records from quickbooks, 
     * updates any changes to the displayName values,
     * and removes any records that are marked as inactive in quickbooks
     * 
     * @return rendered view
     */
    public function actionRefresh() 
    {
        $Intuit = $this->getIntuitObject();
        if ( $this->_pullDataMap( $Intuit ) ) {
            Yii::app()->user->setFlash('success', "Refreshed the entity data from quickbooks." );
        }
        $this->redirect('list');
    }
    /**
     * @return redirect to 
     */
    public function actionUnmap() 
    {
        $afterActionRoute   = "/" . $this->getUniqueId() . "/list";
        $response           = false;

        // TODO -- verify the user is allowed to unmap

        // pull in the parameter
        $remote_id   = intval( Yii::app()->getRequest()->getQuery('rid') );
        $local_id    = intval( Yii::app()->getRequest()->getQuery('lid') );
        $is_ajax     = Yii::app()->getRequest()->isAjaxRequest;

        // set the limits
        $company_id = Yii::app()->user->companyId;
        $criteria = new CDbCriteria;
        $criteria->compare('company_id', $company_id );


        // load the model and let it handle the unmap
        if ( $remote_id > 0 ) {
            $model = $this->getModel()->findByPk( $remote_id, $criteria );
        } else if ( $local_id > 0 ) {
            $model = $this->getModel()->findByAttributes( array( "local_entity_id" => $local_id ), $criteria );
        } 
        if ( ! is_null( $model ) ) {
            $model->clearMapping();
            $response = $model->save();
        } 
        if ( $response ) {
            Yii::app()->user->setFlash("success", "Unmapped " . $model->getDisplayName() . ".");
        } else {
            Yii::app()->user->setFlash("error", "Unable to load remote entity with id " . $remote_id . ".");
        }
        $this->redirect( $afterActionRoute );
    }
    /**
     * for some entities we need to let the user choose various required settings
     * before we can create the remote entities
     * use this function as an intercept to display the input fields
     * 
     * @return [type]
     */
    protected function actionCreateRequiredRemoteAttributes( $local_entity_id ) {}
    /**
     *  gateway into creating entities on either side depending on passed parameters
     *  written primarily as a GET action, but can probably be adjusted slightly to work with ajax as well...
     * 
     * @return view or redirect
     */
    public function actionCreate() 
    {
        $request = Yii::app()->getRequest(); // the CHttpRequest object        
        $remote_id  = intval( $request->getParam('rid') );    // the row id of the remote record -- without the pullData there will not be any data
        $local_id   = intval( $request->getParam('lid') );
        $is_ajax    = $request->isAjaxRequest;
        $model      = $this->getModel(); // ie QboEmployee
        $view       = "/partial_views/_ajax_message";
        $afterActionRoute = "/" . $this->getUniqueId() . "/list";

        $dataService    = $this->getDataService();

        if ( $remote_id > 0 && $local_id > 0 ) {
            throw new CHttpException(400,'It looks like you are trying to map these entities, not create them! Please use the proper list to GUI to do it.');            
        }
        if ( ! $remote_id && $local_id > 0 ) {
            // display a list of special attributes that need to be selected first
            $requiredParameters = $this->actionCreateRequiredRemoteAttributes( $local_id );

            // TODO -- can we do this any better?? app()::end()>??
            if ( $requiredParameters["rendered"] )
                // Yii::app()->end();
                return; // finish the action here

            // now use the local entity setup to create a new Object of that type
            $local_entity_model_name = ucwords( $this->local_entity_name );
            $localEntity = new $local_entity_model_name();
            $localEntity = $localEntity->findByPk( $local_id );

            if ( is_null( $localEntity ) ) {
                throw new CHttpException(500,'Unable to load the referenced local entity. Please use the controls provided by the list view.');                            
            }
            // the helper that pulls qbo data in
            $dataService = $this->getDataService();
            // let the dataService load the remote entity details and assign them to the local entity object
            $remoteEntity = $dataService->AddEntity( $localEntity, $requiredParameters );

            $error = $dataService->getLastErrorMessage();

            if ( ! $error && ! is_null( $remoteEntity ) ) {
                 // now store employees details in our employee table, as we don't want to play name match games later
                 $remote_entity_id = $remoteEntity->Id;
                    
                 // this should return a new record, or if the remote_entity_id exists will give us that
                 $remoteRecord = $this->getModel()->getByRemoteEntityId( $remote_entity_id );
                 $remoteRecord->setDisplayName( $dataService->getDisplayName( $remoteEntity ) );
                 $remoteRecord->setMapping( $local_id );

                 // store last check time, and potential new record
                 if ( !$remoteRecord->hasErrors() && $remoteRecord->save() ) {
                    $data = array("status" => "success", "message" => "Stored the quickbooks mapping as " . $remoteRecord->getDisplayName() );
                 } else {
                     throw new CHttpException(500,'Unable to record quickbook employee data in the database.');
                 }
             } else {
                $data = array("status" => "error", "message" => "Quickbooks error: " . $error );
             }
        }
        if ( $remote_id > 0 && ! $local_id ) {
            // TODO -- implement class parent overwrite to only allow searching for our company_id ??
            $remoteRecord = $model->findByPk( $remote_id );
            if ( is_null( $remoteRecord ) ) {
                throw new CHttpException(500,'Invalid remote_id supplied! Try using the gui for these requests.');
            }
            // now use the local entity setup to create a new Object of that type
            $local_entity_model_name = ucwords( $this->local_entity_name );
            $newLocalEntity = new $local_entity_model_name();
            // the helper that pulls qbo data in
            $dataService = $this->getDataService();
            // let the dataService load the remote entity details and assign them to the local entity object
            $newLocalEntity = $dataService->getLocalEntity( $remoteRecord->remote_entity_id, $newLocalEntity );
            // any issues with parsing should appear here
            $error = $dataService->getLastErrorMessage();
            if ( ! $error ) {
                // now pass the buck to the EntityController child to set any specific attributes
                $newLocalEntity = $this->processLocalEntityCreation( $newLocalEntity );
                if ( ! $newLocalEntity->hasErrors() && $newLocalEntity->save() ) {
                    $remoteRecord->local_entity_id = $newLocalEntity->id;
                    if ( $remoteRecord->save() ) {
                        $data = array("status" => "success", "message" => "Successfully created a new entity and mapped it." );
                    } else {
                        $data = array("status" => "error", "message" => "Error while saving the remote record: " . CHtml::errorSummary( $remoteRecord, "") );
                    }
                } else {
                    $data = array("status" => "error", "message" => "Error while saving the new local record: " . CHtml::errorSummary( $newLocalEntity, "") );
                }
            } else {
               $data = array("status" => "error", "message" => "There was a problem reading the requested remote record: " . CHtml::errorSummary( $newLocalEntity, "") );
            }
        }

        // output only what we need
        if ( $is_ajax ) {
            $this->renderJSON($view, $data );
        } else {
            // unless specifically disabled, the non-ajax requests are redirected
            if ( $afterActionRoute === false ) {
                $this->render($view, $data );
            } else {
                // set flash message
                Yii::app()->user->setFlash($data["status"], $data["message"] );
                $this->redirect( $afterActionRoute );
            }
        }
    }

    /**
     *  mapping action to help glue entities from remote and local 
     *  the paths should look something like:
     *  /qbo/employee/map/rid/xx/lid/yy     - map remote entity with id xx to local entity with id yy
     *  /qbo/employee/map/lid/xx            - display list of remote entities that can map to the local entity with id xx
     *  /qbo/employee/map/rid/xx            - display list of local entities that can map to the remote entity with id xx
     * 
     * @return view or redirect
     */
    public function actionMap() 
    {
        $remote_entity_name = $this->remote_entity_name;
        $local_entity_name  = $this->local_entity_name;
        $cancelRoute        = "/" . $this->getUniqueId() . "/list";
        $createRoute        = "/" . $this->getUniqueId() . "/create";
        $afterActionRoute   = $cancelRoute;

        $view               = false;

        $request = Yii::app()->getRequest(); // the CHttpRequest object
        // $Intuit     = $this->getIntuitObject();
        $remote_id  = intval( $request->getParam('rid') );    // the row id of the remote record -- without the pullData there will not be any data
        $local_id   = intval( $request->getParam('lid') );
        $is_ajax    = $request->isAjaxRequest;
        $model = $this->getModel(); // ie QboEmployee


        // rid is not set && lid is not set -- redirect to employee list OR show a list of all mapped entities?
        if ( ! $remote_id && ! $local_id ) {
            $this->redirect( $afterActionRoute );
        }

        // TODO -- distinguish between submitted form and the ajax/jquery request to render the form...??
        // a clutchy strainer that forces the post data to be used
        if ( $request->isPostRequest ) {
            $modelData = $request->getPost( get_class($model) );
            $action = strtolower( $request->getPost("postaction") );

            // TODO - this still leaves it open to mess with our data if someone passes a POST model knowing the right names...
            if ( ! is_null ( $modelData ) ) {
                $remote_id = isset( $modelData["id"] ) ? intval($modelData["id"]) : 0;
                $local_id  = isset( $modelData["local_entity_id"] ) ? intval($modelData["local_entity_id"]) : 0;
            }
        } 

        // isset(rid) && isset(lid) - assign $lid to row with $rid
        if ( $remote_id > 0 && 
             $local_id > 0 ) {
            if ( ! $request->isPostRequest ) {
                throw new CHttpException(400,'Please use the proper forms to assign entities.');
            }
            // TODO -- validate that the local_id is a legit entity -- do it inside the model instead??
            
            $view = "/partial_views/_ajax_message";
            if ( $model->assignLocalToRemote($remote_id, $local_id ) == 1 ) {
                $data = array("status" => "success", "message" => "Successfully assigned entity." );
            } else {
                // unable to update - check model->lastError for errors?
                $data = array("status" => "error", "message" => "Unable to map the requested entity." );
            }
        }
        // rid not set && lid isset -- show a list of rid's that are available to be mapped
        if ( ! $remote_id  && $local_id > 0 ) { 
            // Yii::app()->user->setFlash('success', "Display remote_id-list for mapping $local_id" );
            $model->setAttribute("local_entity_id", $local_id );        
            $availableDataModels = $model->getAvailableRemoteMatches();

            // display a list of rids to be matched to 
            $view = "/partial_views/map_rid_to_lid";
            $data = array(  "matchModels"   => $availableDataModels,
                            "createRoute"   => $createRoute . "/lid/$local_id" );
            $afterActionRoute = false;
        }
        // rid is set && lid not set -- show a list of lid's that can be mapped to the rid
        if ( $remote_id > 0 && ! $local_id ) {
            // Yii::app()->user->setFlash('success', "Display local_id-list for mapping $remote_id" );
            $model = $model->findByPk( $remote_id );
            if ( ! is_null( $model ) ) {
                $availableDataModels = $model->getAvailableLocalMatches();
                // display a list of lid's to be matched to the rid
                $view = "/partial_views/map_lid_to_rid";
                $data = array(  "matchModels"   => $availableDataModels,
                                "createRoute"   => $createRoute . "/rid/$remote_id" );
                $afterActionRoute = false;
            } else {
                throw new CHttpException(500,'Invalid remote record id. Please use the proper views to map entities.');
            }
        } 
        if ( ! isset( $data ) ) {
            // handle it gracefully when someone tries to pass in parameters that make no sense
            $view = "/partial_views/_ajax_message";
            $data = array("status" => "error", "message" => "Unable load any available entities to map." );            
        } else {
            // add the details for displaying data
            $data = array_merge( $data,
                                 array(  "model"         => $model, 
                                         "cancelRoute"   => $cancelRoute ) );            
        }
        // output only what we need
        if ( $is_ajax ) {
            $this->renderJSON($view, $data );
        } else {
            // unless specifically disabled, the non-ajax requests are redirected
            if ( $afterActionRoute === false ) {
                $this->render($view, $data );
            } else {
                // set flash message
                Yii::app()->user->setFlash($data["status"], $data["message"] );
                $this->redirect( $afterActionRoute );
            }
        }
    }

    /**
     * @param  QboIntuit object
     * @return boolean indicating if the pull was successful
     */
    private function _pullDataMap( $RemoteDataSource ) {
        // $remoteData  = $RemoteDataSource->getEmployeeList();
        $remote_entity_name = $this->remote_entity_name;
        $model_name         = "Qbo" . ucwords( $remote_entity_name );   // ie QboEmployee
        
        $dataService        = $this->getDataService();//new $data_service_name( $RemoteDataSource->getServiceContext() );
        $remoteData         = $dataService->FindAll( $remote_entity_name );
        $model              = new $model_name();        
        $dataMap            = array();  // will contain row-id indexed objects of IPP{EntityName} type that are mapped in our database
        $activeRecordIds    = array();  // any records that we did not see upon refresh need to be elminated

        if ( count( $remoteData ) ) {
            foreach( $remoteData as $index => $item ) {
                // check if their id exists in the database
                $entity_id = $dataService->getEntityId( $item );
                array_push( $activeRecordIds, $entity_id );
                
                $record = $this->getRemoteEntityRecord( $entity_id );
                $record->setDisplayName( $dataService->getDisplayName( $item ) );
                // @ TODO implement this as entity specific action if needed
                // $emailAddress = $dataService->getEmployeeEmail( $item );
                // // see if we can match someone via email
                // if ( $emailAddress ) {
                //     $record->mapUserByEmail( $emailAddress );
                // }

                // store last check time, and potential new record
                if ( $record->save() ) {
                    $dataMap[ $record->id ] = $record;
                } else {
                    throw new CHttpException(500,'Unable to record quickbook entity data in the database.');
                }
            }
        } else {
            if ( $error = $dataService->getLastErrorMessage() ) {
                Yii::app()->user->setFlash('error', "Quickbooks says: $error" );
            } else {
                Yii::app()->user->setFlash('error', "Unable to load any " . $remote_entity_name . " entities from quickbooks." );
            }

        }
        $this->_clearStaleRemoteRecords( $activeRecordIds );
        return $dataMap;
    }
    /**
     * implementation required to handle quickbook entities being marked as "inactive"
     * on their end they are considered deleted, but can still show up unless specifically asked to only show 
     * "active" entities - which is how most of the Qbo Dataservices are setup.
     * 
     * @param  array $activeRecordIds of integer values that should not be cleared
     * @return integer indicating how many records have been cleared
     */
    protected function _clearStaleRemoteRecords( $activeRecordIds )
    {
        $cleared_count = 0;
        $model = $this->getModel();
        $criteria = new CDbCriteria;
        $criteria->compare('company_id', $model->getCompanyId() );

        if ( count ( $activeRecordIds ) ) {
            $criteria->addNotInCondition('remote_entity_id', $activeRecordIds );
        }
        $result = $model->findAll( $criteria );
        if ( count( $result ) ) {
            foreach( $result as $record ) {
                if ( $record->delete() ) {
                    $cleared_count++;
                }
            }
        }
        return $cleared_count;
    }
    /** 
     * @param  array the contains keyed data items
     * @return string that tells the view if the data is mapped
     */
    public function getMappingStatus( $data ) 
    {
        if ( $data["id"] == null || 
             $data["local_entity_id"] == null ) {
            return "Unmapped";
        } else {
            return "Mapped";
        }
    }
    /**
     * @param  array of the griddata
     * @return boolean
     */
    public function isUnmapped( $data )  
    {
        if ( $data["id"] == null || 
             $data["local_entity_id"] == null ) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * an intelligent way to construct the entity mapping and return it as a url
     * @param  array griddata 
     * @return string url that points to the map action for this entity
     */
    public function getMapActionLink( $data ) 
    {
        // create route as controller/action pair as required by createUrl()
        $route = $this->id . "/" . "map";
        $local_entity_id    = $data["local_entity_id"];
        $remote_record_id   = $data["id"];
        $parameters = array();
        // now decide what gets attached 
        if ( ! is_null ( $remote_record_id ) ) {
            $parameters["rid"] = $remote_record_id;
        }
        if ( ! is_null( $local_entity_id ) ) {
            $parameters["lid"] = $local_entity_id;
        }
        // let yii construct a useful url
        $url = $this->createUrl( $route, $parameters );
        return $url;
    }
    /**
     * @param  array $data that is used to populate the _grid_entity_list (CGridView)
     * @return string of the url that points to the unmap action for this entity
     */
    public function getUnmapActionLink( $data ) 
    {
        // create route as controller/action pair as required by createUrl()
        $route = $this->id . "/" . "unmap";
        // get the remote record that needs to be unmapped
        if ( ! isset( $data["id"]) ) {
            throw new CHttpException(500,'There was a problem creating the unmap link for this entity. Please contact your developer.');
        }
        $remote_record_id =  intval($data["id"]);
        // now decide what gets attached 
        if ( $remote_record_id > 0 ) {
            $parameters = array("rid" => $remote_record_id );
        }        
        // let yii construct a useful url
        $url = $this->createUrl( $route, $parameters );
        return $url;
    }
    /**
     * @param  array $data that is used to populate the _grid_entity_list (CGridView)
     * @return string of the url that points to the unmap action for this entity
     */
    public function getCreateActionLink( $data ) 
    {
        // create route as controller/action pair as required by createUrl()
        $route = $this->id . "/" . "create";
        $local_entity_id    = $data["local_entity_id"];
        $remote_record_id   = $data["id"];
        $parameters = array();
        // now decide what gets attached 
        if ( ! is_null ( $remote_record_id ) ) {
            $parameters["rid"] = $remote_record_id;
        }
        if ( ! is_null( $local_entity_id ) ) {
            $parameters["lid"] = $local_entity_id;
        }
        // let yii construct a useful url
        $url = $this->createUrl( $route, $parameters );
        return $url;
    }


    /**
     * a simple constructor for getting the model that belongs to the controller via naming convention
     * Controller           --> Model
     * [Employee]Controller --> Qbo[Employee]
     * 
     * @return QboEntityMap model type (Employee/Project/Customer etc...)
     */
    public function getModel($new = false) 
    {
        if ( ! isset( $this->model ) || $new ) {
            $model_name = "Qbo" . ucwords( $this->id );
            $this->model = new $model_name();
        }
        return $this->model;
    }
    /**
     * a simple constructor for getting the model of the local entity that belongs to the controller 
     * as defined in the controller child
     * 
     * @return Yii model type (User/Task/Item etc...)
     */
    public function getLocalEntityModel() 
    {
        $model_name = ucwords( $this->local_entity_name );
        return new $model_name();
    }
    /**
     * @return Qbo[enity_name]DataService object that contains specific functions to deal with quickbook entities for this entity_name
     */
    protected function getDataService() 
    {
        // TODO -- turn this into a reusable object
        $data_service_name = "Qbo" . ucwords( $this->id ) . "DataService";
         return new $data_service_name( $this->getIntuitObject()->getServiceContext() );        
    }

    /**
     * 
     * @param  integet $remote_entity_id the entity id that identifies a record on the remote quickbooks system
     * @return Qbo[EntityName] model object that we can modify and save to
     */
    protected function getRemoteEntityRecord( $remote_entity_id ) {
        // important to set the model to true, otherwise previously used model id could sneak in if the remote id is not found
        $model = $this->getModel(true);
        return $model->getByRemoteEntityId( $remote_entity_id );
    }


}