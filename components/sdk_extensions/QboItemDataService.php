<?php
/**
 * DataService wrapper class that exposes Employee entity specific functionality
 * 
 * Notes: 
 *      We only focus on Item Types that are a "Service"
 *
 *      Each item has a unique name -- that will be our display name
 *
 *      if we create tasks on their server, think about prefixing them with Hawki to avoid duplicate names?
 *
 * 
 * @author OnCallSolutions
 * @copyright OnCallSolutions
 * @package  qbo yii-module
 * 
 */
use QuickBooksOnline\API\Data\IPPItem;
use QuickBooksOnline\API\Data\IPPItemTypeEnum;

class QboItemDataService extends QboDataService {    
    /**
     * @return string used with query() call to retrieve entities of this type
     */
    protected function getFindAllQueryString() 
    {
        $query = "SELECT * FROM item WHERE type='service' AND active=true";
        return $query;
    }
    /**
     * @param  IPPEmployee
     * @return int
     */
    public function getEntityId( IPPItem $item ) 
    {
        return $item->Id;
    }
    /**
     * @param  IPPEmployee
     * @return string
     */
    public function getDisplayName( IPPItem $item ) 
    {
        return $item->Name;
    }

    /**
     * populate a remote entity object from a local entity model
     * 
     * @param  Task $localEntity that contains the attributes to be created on the remote entity
     * @param array $requiredParameters keyed values that are collected from user (not stored in the mapping table)
     * @return IPPItem that will let the DataService generate a quickbooks entity
     */
    protected function mapLocalFieldsToRemoteEntity( Task $localEntity, $requiredParameters ) 
    {
        $params = array();
        if ( $localEntity->name ) {
            $params["Name"] = $localEntity->name;
        }
        if ( $localEntity->name ) {
            $params["Description"] = $localEntity->description;
        }        
        $params["Type"] = new IPPItemTypeEnum( array( "Type" => 'Service', 'value' => 'Service' ) );
        $params["ServiceType"] = 'service';


        // Quickbooks error: [2020] Required parameter ExpenseAccountRef or IncomeAccountRef is missing in the request
        // because of this error, we must implement extra lookup queries
        if ( isset( $requiredParameters["expense_account_id"] ) ) {
            // rather than building it ourselves, query it and let the SDK do the construction work
            $params["ExpenseAccountRef"] = $this->getAccountReference( $requiredParameters["expense_account_id"], "ExpenseAccountRef" );
        }
        if ( isset( $requiredParameters["income_account_id"] ) ) {
            $params["IncomeAccountRef"] = $this->getAccountReference( $requiredParameters["income_account_id"], "IncomeAccountRef" );
        }
        // turn the activeRecord object parameters into an IPPIntuitEntity        
        return new IPPItem( $params );
    }
    /**
     * just a 'dumb' mapper function -- all field integrity validation should happen in the controller
     * -- makes error processing easier
     * 
     * @param  integer $remote_entity_id the id that identifies the quickbook entity on their server
     * @param  Task $entityRecord Yii model 
     * @return Task filled with attribute values
     */
    protected function mapRemoteEntityToLocalRecord( $entity_id, Task $entityRecord ) 
    {
        $entity = $this->Query("SELECT * FROM Item WHERE id='$entity_id' AND type='service' AND active=true");
        $error = $this->getLastErrorMessage();

        // query response isn't all that great
        if ( ! is_null( $entity ) && count( $entity ) == 1 ) {
            $entity = array_shift( $entity );
            $attributes = array();
            if ( $entity->Name ) {
                $attributes["name"] = $entity->Name;
            }
            if ( $entity->Description ) {
                $attributes["description"] = $entity->Description;
            }
            $entityRecord->attributes = $attributes;
        }
        // return the populated object to the caller, let them deal with saving data and field validation
        return $entityRecord;
    }


    public function getAccounts($accountType) {
        $result = $this->Query("SELECT * FROM account WHERE Classification='$accountType' AND active=true");
        $error  = $this->getLastErrorMessage();

        // TODO -- process errors
        $response = array();
        foreach( $result as $entity ) {
            $response[ $entity->Id ] = $entity->Name;
        }        
        return $response;
    }
    public function getIncomeAccounts() {
        return $this->getAccounts( "Revenue" );
    }
    public function getExpenseAccounts() {
        return $this->getAccounts( "Expense" );
    }

}