<?php
$this->breadcrumbs = array(
  'Quickbooks'  => array('/qbo'),
  Yii::t('app', 'Project Controls'),
);
?>

<?php $this->widget('Flasher'); ?>

<h1>Quickbook Entity Controls - Project</h1>

    <?php 
    /** intuit does not appear to require it, just leaving it here in case things change:
        $this->renderPartial('/_ipp_button', array() ); 
    **/
    ?>

    <p> What would you like to do? </p>
    <p>
        <ul>
            <li><?php echo CHtml::link(' List the existing mappings between Hawki Projects and (un)mapped Quickbooks Customer-Jobs', $this->createAbsoluteUrl('/qbo/customer/list')); ?></li>
        </ul>
    </p>