<?php

use GuzzleHttp\Exception\BadResponseException;
use League\OAuth1\Client\Server\Server;
use League\OAuth1\Client\Server\User;
use League\OAuth1\Client\Credentials\TokenCredentials;
use League\OAuth1\Client\Credentials\CredentialsException;

class QboOauth1Client extends Server
{
    // const EXPIRY_EXPIRED = 'expired';
    // const EXPIRY_NOTYET = 'notyet';
    // const EXPIRY_SOON = 'soon';
    // const EXPIRY_UNKNOWN = 'unknown';

    protected $responseType = 'xml';

    public function urlTemporaryCredentials()
    {
        return QboConnection::URL_REQUEST_TOKEN;
    }

    public function urlAuthorization()
    {
        return QboConnection::URL_CONNECT_BEGIN;
    }

    public function urlTokenCredentials()
    {
        return QboConnection::URL_ACCESS_TOKEN;
    }

    public function urlUserDetails()
    {
        return QboConnection::URL_CURRENT_USER;
    }
    public function urlDisconnect() {
        return QboConnection::URL_CONNECT_DISCONNECT;
    }
    public function userDetails($data, TokenCredentials $tokenCredentials)
    {
        $user = new User;

        $user->firstName = (string) $data->User->FirstName;
        $user->lastName  = (string) $data->User->LastName;
        $user->name      = $user->firstName . ' ' . $user->lastName;
        $user->email     = (string) $data->User->EmailAddress;

        $verified = filter_var((string) $data->User->IsVerified, FILTER_VALIDATE_BOOLEAN);

        $user->extra = compact('verified');

        return $user;
    }

    public function userUid($data, TokenCredentials $tokenCredentials)
    {
        return;
    }

    public function userEmail($data, TokenCredentials $tokenCredentials)
    {
        return (string) $data->User->EmailAddress;
    }

    public function userScreenName($data, TokenCredentials $tokenCredentials)
    {
        return;
    }
    /**
     * Handle a bad response coming back when getting token credentials.
     *
     * @param BadResponseException $e
     *
     * @throws CredentialsException
     */
    protected function handleTokenCredentialsBadResponse(BadResponseException $e)
    {
        $response = $e->getResponse();
        $body = $response->getBody();
        $statusCode = $response->getStatusCode();

        throw new CHttpException($statusCode, "Yii Received HTTP status code [$statusCode] with message \"$body\" when getting token credentials.");
    }
    /**
     * 
     * @param TemporaryCredentials|string $temporaryIdentifier
     *
     * @return string
     */
    public function getDisconnectUrl( $app_token ) {
        $clientCredentials = $this->clientCredentials;

        $parameters = array('app_token'          => $app_token,   
                            'oauth_access_token' => $clientCredentials->getIdentifier(),
                            'oauth_secret_token' => $clientCredentials->getSecret() );

        $url = $this->urlDisconnect();
        $queryString = http_build_query($parameters);

        return $this->buildUrl($url, $queryString);
    }

    /**
     * Hit the disconnect url
     *
     * @param 
     */
    public function disconnect( $app_token ) {
        $url = $this->getDisconnectUrl( $app_token );

        header('Location: '.$url);

        return;
    }
}