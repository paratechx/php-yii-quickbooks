<div id="installer" class="confirm">

	<h2><?php echo Yii::t('install', 'Install Quickbooks Online Module'); ?></h2>

	<p><?php echo Yii::t('install', 'Please confirm if you wish to install this module.'); ?></p>

	<p>
		<?php echo CHtml::link(Yii::t('install', 'Yes'), array('install/run', 'confirm'=>1)); ?> /
		<?php echo CHtml::link(Yii::t('install', 'No'), Yii::app()->homeUrl); ?>
	</p>

</div>