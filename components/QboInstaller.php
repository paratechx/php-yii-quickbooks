<?php
/**
 * helper component to facilitate the sql table installation
 *
 * @author OnCallSolutions
 * @copyright OnCallSolutions
 * 
 */
class QboInstaller extends CApplicationComponent
{
    const ERROR_NONE=0;
    const ERROR_QUERY_FAILED=1;
    const DEVMODE=true;

    public $db;
    private $_refreshLog = array();
    private $_installed;
    // bundle names for automation of validation code ModelName => database_table_name
    private $requiredTables = array( 'QboToken'         => "qbo_token",
                                     'QboEmployee'      => "qbo_employee_to_user_map",
                                     'QboCustomer'      => "qbo_customer_to_project_map",
                                     'QboItem'          => "qbo_item_to_task_map",
                                     'QboTimeactivity'  => "qbo_timeactivity_to_companytotals_map",
                                    );    
	public function init()
	{
		parent::init();
		$this->db = Yii::app()->db;
        if ( self::DEVMODE ) {
            $this->_verifyTableInstalls( self::DEVMODE );
        }
	}
	/**
     * Runs the installer.
     * 
     * @return integer as defined indicating success or failure
     */
    public function run()
	{
        // Fetch the schema.
        $schema = file_get_contents(dirname(__FILE__).'/../data/schema.sql');

        // Correct the table names. ???
        $schema = strtr($schema,  $this->requiredTables );

        // Convert the schema into an array of sql queries.
        $schema = preg_split("/;\s*/", trim($schema, ';'));

        // Start transaction
        $txn = $this->db->beginTransaction();

        try
        {
            // Execute each query in the schema.
            foreach( $schema as $sql )
            {
                $command = $this->db->createCommand($sql);
                $command->execute();
            }

            // All commands executed successfully, commit.
            $txn->commit();
            return self::ERROR_NONE;
        }
        catch( CDbException $e )
        {
            // Something went wrong, rollback.
            $txn->rollback();
            return self::ERROR_QUERY_FAILED;
        }
	}

	/**
     * do some checks if we need to install anything
     * 
 	 * @return boolean whether QBO tables are installed.
	 */
	public function isInstalled() 
    {		
		if( $this->_installed!==null ) {
			return $this->_installed;
		} else {
			return $this->_installed = $this->_verifyTableInstalls();
		}
	}
    /**
     * verifies that the tables are there as defined in $this->requiredTables
     * 
     * @param  boolean $attemptRefresh a simple way to permit reloading of the tables if they dont exist 
     * @return boolean true if all tables exist
     */
    private function _verifyTableInstalls( $attemptRefresh = true ) 
    {
        $sqlFunction = function($value) {
            return "SELECT COUNT(*) FROM {$value}"; 
        };

        $schema = array_map( $sqlFunction, $this->requiredTables );

        foreach( $schema as $modelName => $sql ) {
            $installed = true;
            try {
                $command = $this->db->createCommand($sql);
                $command->queryScalar();
            }
            catch( CDbException $e ) {
                $installed = false;
                // try one more time to setup the tables if allowed
                if ( $attemptRefresh ) {
                    $installed = $this->_refreshTableScheme( $modelName );
                } 
                // can't fix it by now, leave the function
                if ( ! $installed ) {
                    break;
                }
            }
        }
        return $installed;
    }
    /**
     * a very simple minded way to refresh only particular tables
     * useful to leave the oauth tokens intact while still developing the rest of the code
     * 
     * @param  string $modelname that must be in the $requiredTables array!
     * @return integer query success indication via class constants defined up top
     */
    private function _refreshTableScheme( $modelName )
    {
        // Fetch the schema.
        $schema = file_get_contents(dirname(__FILE__).'/../data/schema.sql');
        // keep track which models get updated
        $refreshLog = array();
        // Convert the schema into an array of sql queries.
        $schema = preg_split("/;\s*/", trim($schema, ';'));
        // Start transaction
        if ( is_null ( $txn = $this->db->getCurrentTransaction() ) )
            $txn = $this->db->beginTransaction();

        try {
            // look at each each query in the schema.
            foreach( $schema as $sql ) {
                // but only execute, if it contains the particular model name reference
                if ( strpos( $sql, $modelName ) ) { 
                    $sql = str_replace($modelName, $this->requiredTables[$modelName], $sql);

                    $command = $this->db->createCommand($sql);
                    $command->execute();
                    // store as name and action keyword (SELECT/DELETE etc...)
                    $refreshLog[] = $modelName . " - " . strtok($sql, " ");
                }
            }
            // All commands executed successfully, commit.
            $txn->commit();
            // store the result in this component for potential log messages
            $this->_refreshLog = array_merge( $this->_refreshLog, $refreshLog);
            return self::ERROR_NONE;
        }
        catch( CDbException $e ) {

            // Something went wrong, rollback.
            $txn->rollback();
            return self::ERROR_QUERY_FAILED;
        }
    }
    /**
     *  a simple helper to update tables during dev without loosing the access tokens
     * 
     * @return boolean
     */
    public function refreshSelectedTables() 
    {
        $this->_refreshTableScheme("QboTimeactivity");
        // $this->_refreshTableScheme("QboEmployee");

        // $this->_refreshTableScheme("QboCustomer");
        // $this->_refreshTableScheme("QboItem");

        return true;
    }
    /**
     * use it in conjunction with yiis flash functions to provide some feedback
     * 
     * @return array with the record of which tables were refreshed
     */
    public function getRefreshLog() 
    {
        return $this->_refreshLog;
    }
}
