<?php
$this->breadcrumbs = array(
  'Quickbooks'  => array('/qbo'),
  Yii::t('app', 'Timeactivity Controls'),
);
?>

<?php $this->renderPartial('/_ipp_button'); ?>
<?php $this->widget('Flasher'); ?>

<h1>Quickbook Entity Controls - Timeactivity</h1>

    <p> What would you like to do? </p>
    <p>
        <ul>
            <li><?php echo CHtml::link('Display Mapping List of Timeactivities', $this->createAbsoluteUrl('/qbo/timeactivity/list')); ?></li>
            <?php
            /**
            <li><?php echo CHtml::link(' Refresh available employee entities.', $this->createAbsoluteUrl( '/qbo/employee/refresh' ) ); ?></li> 
            <li><?php echo CHtml::link(' Create all <strong>unmapped Hawki Users</strong> as Employee entities in Quickbooks Online', $this->createAbsoluteUrl( $urlMapLocals ) ); ?></li> 
            <li><?php echo CHtml::link(' List the existing mappings between Hawki Users and (un)mapped Quickbooks Employees', $this->createAbsoluteUrl('/qbo/employee/list')); ?></li>            
            **/?>
        </ul>
    </p>