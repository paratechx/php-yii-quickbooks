<?php
/** 
 *  HoursProjectTaskController has the sql mix
 *
 *
 *
 * 
 */
class QboTimeactivity extends QboEntityMap
{
    protected $entity_table_name    = "companytotals";
    protected $tableName            = "qbo_timeactivity_to_companytotals_map";    

    protected $entityMapColumns     = array("id", "local_entity_id", "company_id", "remote_entity_id", "remote_display_name");
    protected $entityColumns        = array( NULL, "id", "company_id", NULL, NULL); // must match the count and type (or NULL) of the entityMapColumns


    public $to_date;
    public $from_date;

    public $ctid;
    public $user_name; 
    public $activity_date;

    public $project_name; 
 
    public $activity_minutes;
    public $remote_project_map_id; 
    public $remote_user_map_id; 
    public $allow_export;

    public $project_id;


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            // a phony relationship, but keeping it for now
            'companytotals' => array(self::BELONGS_TO, 'Companytotals', 'local_entity_id'),
            // used to retrieve queued data values
            'project'       => array(self::BELONGS_TO, 'Project', 'project_id'),
            'employee'      => array(self::BELONGS_TO, 'User', 'user_id'),
            // relying on the foreign key to join rather than the primary key
            'user_entity_map'      => array(self::HAS_ONE, 'QboEmployee', '',
                                     'on'=>'user_entity_map.local_entity_id = t.user_id',
                                     'condition'=>'user_entity_map.allow_export = true AND project_entity_map.company_id = ' . $this->getCompanyId() ),
            'project_entity_map'   => array(self::HAS_ONE, 'QboCustomer', '',
                                     'on'=>'project_entity_map.local_entity_id = t.project_id',
                                     'condition'=>'project_entity_map.company_id = ' . $this->getCompanyId()  ),
        );
    }

    /**
     * @return array 
     */
    public function rules() 
    {
        return array(
            array('activity_date, to_date,from_date', 'type', 'type' => 'date', 'message' => '{attribute}: is not a date!', 'dateFormat' => 'yyyy-MM-dd' ),
            array('project_name', 'safe'),


            array('id','safe'),
            array('local_entity_id','safe'),
            array('company_id','safe'),
            array('remote_entity_id', 'safe'),
            array('remote_display_name', 'safe'),
            array('allow_export', 'safe'),
            array('create_time,create_user_id, update_time, update_user_id', 'safe', 'on'=>'search'),            
        );
    }

    /**
     * @return array customizing the field labels to this particular entity
     */
    public function attributeLabels() 
    {
        return array(
            'id'                     => Yii::t('app', 'ID'),
            'local_entity_id'        => Yii::t('app', 'Mapped Project'),
            'company_id'             => Yii::t('app', 'Company'),
            'remote_entity_id'       => Yii::t('app', 'Quickbooks CustomerJob'),
            'remote_display_name'    => Yii::t('app', 'Quickbooks Name'),
            'create_time'            => Yii::t('app', 'Create Time'),
            'create_user_id'         => Yii::t('app', 'Create User'),
            'update_time'            => Yii::t('app', 'Update Time'),
            'update_user_id'         => Yii::t('app', 'Update User'),
            'activity_date'          => Yii::t('app', 'Activity Date')
        );
    }
    /** 
     *  must be in here, else breaks functionality...??
     * 
     * @param string of class name
     * @return static active record model instance.
     */
    public static function model($className=__CLASS__) 
    {
        return parent::model($className);
    }
    /**
     *  overwrite as the project __toString() doesn't output useful information
     * 
     * @return anonymous function that can be passed to listdata or other functions
     */
    public function getEntityDisplayNameCreator() 
    {
        $fn = function($entityObject) { return CHTML::encode( "Shiftid: $entityObject->shiftid [ $entityObject->id ] "); };
        return $fn;
    }
    /**
     * @return QboEntityMapProvider provides data in terms of arrays, each representing a row of query result.
     */
    public function getExportData() 
    {
        $where = array( "ct.company_id = '10329'" );

        if ( $this->from_date ) {
            $where[] = "ct.starttime > " . strtotime( $this->from_date );
        }
        if ( $this->to_date ) {
            $where[] = "ct.starttime < " . strtotime( $this->to_date );
        }

        $where[] = "ct.direct_mins > 0";

        // TODO - this might cause some confusion
        $where[] = "(umap.allow_export = true OR umap.allow_export IS NULL)";

        $where = implode( " AND ", $where);

        // allows for much easier development in conjunction with mysql/adminer...
        $sql = "SELECT 
                                                                ct.id AS ctid,
                                      CONCAT(u.lastname, u.firstname) AS user_name, 
                 DATE_FORMAT(FROM_UNIXTIME(ct.starttime), '%Y-%m-%d') AS activity_date, 
                                                               p.name AS project_name,                                                               
                                                SUM( ct.direct_mins ) AS activity_minutes, 
                                                              pmap.id AS remote_project_map_id, 
                                             pmap.remote_display_name AS remote_project_display_name,
                                                pmap.remote_entity_id AS remote_project_entity_id, 
                                                              umap.id AS remote_user_map_id, 
                                             umap.remote_display_name AS remote_user_display_name,
                                                umap.remote_entity_id AS remote_user_entity_id, 
                                                    umap.allow_export AS allow_export,
                                                    ct.*
                FROM companytotals AS ct
                JOIN project AS p ON p.id = ct.project_id
                JOIN user AS u ON u.id = ct.employee_id
                JOIN task AS t ON t.id = ct.task_id
                LEFT JOIN qbo_customer_to_project_map AS pmap ON p.id = pmap.local_entity_id
                LEFT JOIN qbo_employee_to_user_map    AS umap ON u.id = umap.local_entity_id
                WHERE $where
                GROUP BY ct.employee_id, activity_date, ct.project_id 
        ";

        $countSql = "SELECT COUNT(*) FROM ( $sql ) AS count_alias";
        $unmappedCountSql = $countSql . " WHERE remote_user_map_id IS NULL OR remote_project_map_id IS NULL";

        $totalCount = Yii::app()->db->createCommand( $countSql )->queryScalar();
        $hawkiUnmappedCount = Yii::app()->db->createCommand( $unmappedCountSql )->queryScalar();

        $sort = array(
                'attributes'=>array('ctid'),
                'defaultOrder' => array( 'user_name'=> CSort::SORT_ASC, 'activity_date'=> CSort::SORT_ASC  ),
            );

        $pagination = array('pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']), );
        $key_field = "ctid";

        $model = new QboEntityMapProvider( $sql, array(
            'keyField'              => $key_field,
            'totalItemCount'        => $totalCount, 
            'hawkiUnmappedCount'    => $hawkiUnmappedCount,
            'sort'                  => $sort,
            'pagination'            => $pagination
        ));
        return $model;
    }
    public function findAllStaleActivities() 
    {
        $criteria = new CDbCriteria;
        $criteria->compare('`t`.company_id', $this->getCompanyId() );
        $criteria->compare('`t`.active', true );

        if ( $this->from_date ) {
            $fromDate = date("Y-m-d", strtotime($this->from_date) );
            $criteria->addCondition("activity_date >= '$fromDate'" );
        }

        if ( $this->to_date ) {
            $toDate = date("Y-m-d", strtotime($this->to_date) );
            $criteria->addCondition("activity_date <= '$toDate'" );
        }

        // $criteria->addCondition('showmethesql');

        $result = $this->findAll( $criteria );

        return $result;
    }
    public function history()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('`t`.company_id', $this->getCompanyId() );
        //$criteria->compare('`t`.active', true );

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
            'sort'=>array(
                'defaultOrder'=>'create_time DESC',
            )
        ));
    }
    /**
     * provide the controller with a recordset of all timeactivity-maps that have been prepared for submission
     * @return array
     */
    public function getActiveQueue() 
    {
        $criteria = new CDbCriteria;
        $criteria->compare('`t`.company_id', $this->getCompanyId() );
        $criteria->compare('`t`.active', true );
        $criteria->compare('`t`.queued', true );
        // $criteria->addCondition('showmethesql');

        $result = $this->with('user_entity_map', 'project_entity_map')->findAll( $criteria );
        return $result;        
    }
    /**
     * simple helper to format the minute column into its individual pieces  
     * modify the format to get individual values
     * 
     * eg: defaultFormat -- 65 minutes -> 1hrs 5 mins
     *     format = 'H'  -- 65 minutes -> 1
     *     format = 'i'  -- 65 minutes -> 5
     * @param  integer $minutes seed value
     * @param  string $format   how to translate the minute value
     * @return string           formatted output of minute value
     */
    public function formatMinutes( $minutes, $format = "H \h\\r\s i \m\i\\n\s" ) 
    {
        // adjust format and take intval to get the actual values for H and i
        return date($format, mktime(0, intval($minutes)));
    }
    /**
     * using it for the TimeactivityDataService to gain access to the remote id of the relational data
     * @return integer|boolean referencing an entity by id on the quickbooks server
     */
    public function getRemoteEmployeeEntityId() 
    {
        if ( isset( $this->user_entity_map ) )
            return $this->user_entity_map->remote_entity_id;
        else 
            return false;
    }    
    /**
     * using it for the TimeactivityDataService to gain access to the remote id of the relational data
     * @return integer|boolean referencing an entity by id on the quickbooks server
     */
    public function getRemoteCustomerEntityId() 
    {
        if ( isset( $this->project_entity_map ) )
            return $this->project_entity_map->remote_entity_id;
        else 
            return false;
    }    
}
