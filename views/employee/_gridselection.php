<?php

Yii::app()->clientScript->registerScript('mysearch', "
  $(document).delegate('#mysearchbtn', 'click', function(){
    var htmlStr = $(this).html(); 
    if(htmlStr == 'Search')
    {
      $('tr.filters').show();
      $(this).html('Search');
    }
    else
    {
      //$('#fullname').val('');
      //$('#fstatus').val('0');
      $('tr.filters').hide();
      $(this).html('Search');
    }
    return false;
  });
");

?>
<?php
  $this->widget('application.widgets.EPageSize', array(
    'gridViewId' => 'user-grid',
    'beforeLabel'=> 'Users per page',
    'pageSize' => Yii::app()->request->getParam('pageSize',null),
    'defaultPageSize' =>  25 ,   // may use this :  Yii::app()->params['defaultPageSize'],
    'pageSizeOptions'=> array(5=>5, 10=>10, 25=>25, 50=>50, 75=>75, 100=>100), // you can config it in main.php under the config dir . Yii::app()->params['pageSizeOptions'],// Optional, you can use with the widget default
  ));
?>
<?php
  $pageSize = Yii::app()->user->getState('pageSize',25/*Yii::app()->params['defaultPageSize']*/);
  $dataProvider->getPagination()->setPageSize($pageSize);
?>
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
  'id'=>'user-grid',
  'dataProvider'=>$dataProvider,
  'cssFile'=>false,
  'filter'=>$filter,
  'ajaxUpdate'=>false,
  'afterAjaxUpdate'=>'function() { $(".mysearchbtn").click(); }',
  'columns'=>array(
    array(
      'header'  => 'User Name',
      'type'    => 'raw',
      'name'    => 'user_id',
      'value'   => '$data->getCFullname();'
    ),
    array(
      'header'  => 'Action',
      'type'    => 'raw',
      'name'    => 'user_id',
      'value'   => 'CHtml::link( "' . $actionLink["label"] . '", array("' . $actionLink["base"] . '", "uid" => $data->id ), array( "title" => "' . $actionLink["alttitle"] . '") );',
    ),    
  )
));
?>
