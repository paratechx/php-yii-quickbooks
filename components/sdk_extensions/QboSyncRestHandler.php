<?php
/**
 * Class QboSyncRestHandler
 *
 * Extending the SDK class because it still contains plenty buggieness
 *
 * SyncRestHandler contains the logic for preparing the REST request, calls REST services and returns the response.
 * @author OnCallSolutions
 * @copyright OnCallSolutions
 * @package  qbo yii-module
 *
 */
use QuickBooksOnline\API\Core\HttpClients\SyncRestHandler;

class QboSyncRestHandler extends SyncRestHandler
{
    /**
     * Log API Reponse to the Log directory that user specified.
     * @param String $body The requestBody
     * @param String $requestUri  The URI for this request
     * @param Array $httpHeaders  The headers for the request
     */
    public function LogAPIResponseToLog($body, $requestUri, $httpHeaders){
        if ( strpos( $requestUri, "AppMenu" ) === false ) {
            return parent::LogAPIResponseToLog($body, $requestUri, $httpHeaders);
        } else {
            // the AppMenu call does not return XML, which breaks the logging call, so we bypass it this way!
        }
    }
}