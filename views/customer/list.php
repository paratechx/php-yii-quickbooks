<?php
$this->breadcrumbs = array(
  'Quickbooks'  => array('/qbo'),
  'CustomerJobs'   => array('/qbo/customer'),
  Yii::t('app', 'Mapped List'),
);
?>
<?php $this->renderPartial('/_ipp_button', array() ); ?>
<?php $this->widget('Flasher'); ?>

<h1>Quickbook CustomerJobs Map</h1>

<h4> This list contains all mapped and unmapped hawki-projects and quickbooks-customer.jobs assigned to this company. To refresh the listing use the controls below:</h4>

<?php 

    $this->renderPartial('/partial_views/_statistics_entity_list', array('dataProvider'=>$dataProvider ) ); 
    $this->renderPartial('/partial_views/_controls_entity_list', array('dataProvider'=>$dataProvider ) ); 
    $this->renderPartial('/partial_views/_grid_entity_list', $_data_ ); 
?>
