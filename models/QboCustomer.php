<?php
/** 
 * model that builds the entity connection between 
 * QUICKBOOKS-Customer.Jobs and HAWKI-Projects
 *
 * it is a JActiveRecord child with some special sauce from the QboEntityMap parent
 */
class QboCustomer extends QboEntityMap
{
    protected $entity_table_name    = "project";
    protected $tableName            = "qbo_customer_to_project_map";

    /**
     * @return array customizing the field labels to this particular entity
     */
    public function attributeLabels() 
    {
        return array(
            'id'                     => Yii::t('app', 'ID'),
            'local_entity_id'        => Yii::t('app', 'Mapped Project'),
            'company_id'             => Yii::t('app', 'Company'),
            'remote_entity_id'       => Yii::t('app', 'Quickbooks CustomerJob'),
            'remote_display_name'    => Yii::t('app', 'Quickbooks Name'),
            'create_time'            => Yii::t('app', 'Create Time'),
            'create_user_id'         => Yii::t('app', 'Create User'),
            'update_time'            => Yii::t('app', 'Update Time'),
            'update_user_id'         => Yii::t('app', 'Update User'),
        );
    }
    /** 
     *  must be in here, else breaks functionality...??
     * 
     * @param string of class name
     * @return static active record model instance.
     */
    public static function model($className=__CLASS__) 
    {
        return parent::model($className);
    }
    /**
     *  overwrite as the project __toString() doesn't output useful information
     * 
     * @return anonymous function that can be passed to listdata or other functions
     */
    public function getEntityDisplayNameCreator() 
    {
        $fn = function($entityObject) { return CHTML::encode( "[" . $entityObject->id . "] " . $entityObject->name ); };
        return $fn;
    }

}
