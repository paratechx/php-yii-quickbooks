<?php
$this->breadcrumbs = array(
  'Quickbooks'  => array('/qbo'),
  'Employees'   => array('/qbo/employee'),
  Yii::t('app', 'Mapped List'),
);
?>
<?php $this->renderPartial('/_ipp_button', array() ); ?>
<?php $this->widget('Flasher'); ?>

<h1>Quickbook Employees Map</h1>

<h4> This list contains all mapped and unmapped hawki-users and quickbook-employees assigned to this company. To refresh the listing use the controls below:</h4>

<?php 
    $entityColumns = array(
      array(
        'header'  => "Export",
        'type'    => 'raw',
        'name'    => 'allow_export',
        'value'   => '$this->grid->controller->getExportActionLink( $data );',
      )
    );


    $this->renderPartial('/partial_views/_statistics_entity_list', array('dataProvider'=>$dataProvider ) ); 
    $this->renderPartial('/partial_views/_controls_entity_list', array('dataProvider'=>$dataProvider ) ); 
    $this->renderPartial('/partial_views/_grid_entity_list', array_merge( $_data_, array("entityColumns" => $entityColumns)) ); 
?>
