<?php

/**
 * a simple extension that allows me to add additional count values
 * for handling the MapUniverse dataset
 */
class QboEntityMapProvider extends CSqlDataProvider {
    // the trick with these is to provide the set & get methods, then we can add any property as needed
    private $_hawkiUnmappedCount;
    private $_qbUnmappedCount;
    private $_mappedEntityCount;

    /**
     * @param integer count of unmapped hawki entities 
     */
    public function setHawkiUnmappedCount( $value ) 
    {
        $this->_hawkiUnmappedCount = $value;
    }
    /**
     * @return integer indicating the number of unmapped hawki entities
     */
    public function getHawkiUnmappedCount() 
    {
        return $this->_hawkiUnmappedCount;        
    }
    /**
     * @param integer count of unmapped quickbooks entities 
     */
    public function setQbUnmappedCount( $value ) 
    {
        $this->_qbUnmappedCount = $value;
    }
    /**
     * @return integer indicating the number of unmapped quickbooks entities
     */    
    public function getQbUnmappedCount() 
    {
        return $this->_qbUnmappedCount;        
    }
    /**
     * @param integer count of mapped entities 
     */
    public function setMappedEntityCount( $value ) 
    {
        $this->_mappedEntityCount = $value;
    }
    /**
     * @return integer indicating the number of mapped entities
     */    
    public function getMappedEntityCount() 
    {
        return $this->_mappedEntityCount;        
    }
    /**
     * note this does not count! it just depends on the counts being set during initialization of the object
     * 
     * @return boolean indicating if the counts for unmapped entities are there
     */
    public function hasUnmappedData() 
    {
        if ( $this->_hawkiUnmappedCount > 0 || $this->_qbUnmappedCount > 0 ) {
            return true;
        } else {
            return false;
        }
    }
}

