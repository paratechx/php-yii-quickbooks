<div id="installer" class="error">

	<h2><?php echo Yii::t('install', 'Error'); ?></h2>

	<p class="red-text">
		<?php echo Yii::t('install', 'An error occurred while installing Quickbooks Online module.'); ?>
	</p>

    <p>
		<?php echo Yii::t('install', 'Please try again or review the module code.') ;?>
	</p>

</div>