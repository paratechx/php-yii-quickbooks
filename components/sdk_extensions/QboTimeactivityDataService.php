<?php

use QuickBooksOnline\API\Data\IPPTimeactivity;
use QuickBooksOnline\API\Data\IPPTimeActivityTypeEnum;

// use QuickBooksOnline\API\Data\IPPItemTypeEnum;

use QuickBooksOnline\API\Data\IPPReferenceType;
// use QuickBooksOnline\API\Data\IPPAccount;

/**
 *  Notes: 
 *      We only focus on Item Types that are a "Service"
 *
 *      Each item has a unique name -- that will be our display name
 *
 *      if we create tasks on their server, think about prefixing them with Hawki to avoid duplicate names?
 *
 *
 *
 *
 * 
 * DataService wrapper class that exposes Employee entity specific functionality
 */
class QboTimeactivityDataService extends QboDataService {    
    /**
     * @return string used with query() call to retrieve entities of this type
     */
    protected function getFindAllQueryString() 
    {
        $query = "SELECT * FROM timeactivity WHERE active=true";
        return $query;
    }
    /**
     * populate a remote entity object from a local entity model
     * https://developer.intuit.com/docs/api/accounting/timeactivity
     *
     * required fields:
     * - NameOf: required
     * - EmployeeRef: required if NameOf is set to Employee
     * #- VendorRef: required if NameOf is set to Vendor
     * - CustomerRef: required if BillableStatus is set to Billable
     * #- HourlyRate: required if BillableStatus is set to Billable
     * - Hours: Minutes: required if StartTime and EndTime not specified
     * - StartTime: EndTime: required if Hours and Minutes not specified
     *
     * 
     * @param  $localEntity that contains the attributes to be created on the remote entity
     * @param array $requiredParameters keyed values that are collected from user (not stored in the mapping table)
     * @return IPPTimeactivity that will let the DataService generate a quickbooks entity
     */
    protected function mapLocalFieldsToRemoteEntity( QboTimeactivity $localEntity, $requiredParameters = array() ) {
        $params = array();

        // TODO -- make sure the date is the correct format...Local timezone: YYYY-MM-DD, UTC: YYYY-MM-DDZ 
        $params["TxnDate"] = $localEntity->activity_date;

        // only supporting employees at the moment
        $params["NameOf"] = new IPPTimeActivityTypeEnum(array("value" => "Employee"));

        $employee_id = $localEntity->getRemoteEmployeeEntityId();
        if ( $employee_id )
            $params["EmployeeRef"] = new IPPReferenceType( array("type" => "Employee", "value" => $employee_id ));

        $customer_id = $localEntity->getRemoteCustomerEntityId();
        if ( $customer_id ) 
            $params["CustomerRef"] = new IPPReferenceType( array("type" => "Customer", "value" => $customer_id ));
    
        $params["Hours"]    = $localEntity->formatMinutes( $localEntity->activity_minutes, "H");
        $params["minutes"]  = $localEntity->formatMinutes( $localEntity->activity_minutes, "i");

        $params["Description"] = "[auto created by user-initiated action on hawk-i-server app]" 
                               . " Data represents daily hour summary of the following hawki records: " . $localEntity->remote_display_name;

        // turn the activeRecord object parameters into an IPPIntuitEntity        
        return new IPPTimeactivity( $params );
    }
    /**
     * although a SyncToken is mandatory, DELETE on TimeActivities does not appear to check the value of it...
     * 
     * @param  integer $id of the remote record 
     * @param  string $sync_token that identifies the "version" of the remote record
     * @return null|Exception
     */
    public function deleteById($id, $sync_token = "0")
    {
        $entity = new IPPTimeactivity( array( "Id" => intval($id), "SyncToken" => $sync_token ));

        return parent::deleteById($entity);
    }
}