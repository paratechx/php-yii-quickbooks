<?php
/**
 * Provides connectivity functions and setting values for interaction with Intuit Quickbooks Online SDK
 * Used to centralize the various urls that are used for connecting, making it easier to update them when 
 * the SDK changes
 *
 * NOTE: contains a lot of extra constants that are not all in use
 *
 * @author OnCallSolutions
 * @copyright OnCallSolutions
 * @package  qbo yii-module
 * 
 */
class QboConnection
{
    /////////////////////////////////////////////////////////////////////////////
    // MODULE SPECIFIC PATHS - these are used to construct the Oauth1 authentication flow
    const QBO_SCHEME                     = "http";
    const QBO_SLUG_OAUTH                 = "/qbo/oauth";
    const QBO_SLUG_OAUTH_GRANT           = self::QBO_SLUG_OAUTH . "/grant";
    const QBO_SLUG_OAUTH_SUCCESS         = self::QBO_SLUG_OAUTH . "/success";
    const QBO_SLUG_OAUTH_MENU            = self::QBO_SLUG_OAUTH . "/menu";  // TODO --- menu proxy -- what's that for?!
    const QBO_SLUG_OAUTH_DISCONNECT      = self::QBO_SLUG_OAUTH . "/disconnect";
    const QBO_SLUG_OAUTH_RECONNECT       = self::QBO_SLUG_OAUTH . "/reconnect";


    /////////////////////////////////////////////////////////////////////////////
    // LOCAL VM DEVELOPMENT ONLY
    // for local vm development, setup the intuit developer dashboard with these values (settings tab)
    const QBO_LOCALDEV_HOST_DOMAIN 		= "lvh.me";
    const QBO_LOCALDEV_LAUNCH_URL 		= "http://lvh.me/qbo";
    const QBO_LOCALDEV_DISCONNECT_URL	= "http://lvh.me/" . self::QBO_SLUG_OAUTH_DISCONNECT;
    // then configure your /etc/hosts to pass lvh.me to the hawki server
    
    
    /////////////////////////////////////////////////////////////////////////////
    // DEVELOPMENT SANDBOX
    //
    // -- api details found at developer dashboard https://developer.intuit.com/v2/ui#/app/dashboard
    // Accounting Sandbox Base Url
    const INTUIT_SANDBOX_API_URL        = "https://sandbox-quickbooks.api.intuit.com/";
    // Payments Sandbox Base Url - not used, just keeping it for completeness
    const PAYMENTS_SANDBOX_API_URL		= "https://sandbox.api.intuit.com";
    // -- actual sandbox gui can be found at:
    const INTUIT_SANDBOX_DASHBOARD		= "https://developer.intuit.com/v2/ui#/sandbox";

    // shows current selected companies sandbox dashboard (change company by clicking top-right settings icon...)
	const INTUIT_SANDBOX_GUI_URL        = "https://sandbox.qbo.intuit.com/";	

	/////////////////////////////////////////////////////////////////////////////
    // PRODUCTION DETAILS
    //
    // TODO -- is this correct?
    const INTUIT_PRODUCTION_API_URL      = false; // ?? "https://quickbooks.api.intuit.com/";  
    const INTUIT_PRODUCTION_GUI          = false; // @TODO - figure this one out via settings presumably once the app is approved for production

    const INTUIT_GUI_URL                 = self::INTUIT_SANDBOX_GUI_URL;
    // current companies entity listings
    const INTUIT_GUI_EMPLOYEE           = self::INTUIT_GUI_URL . "app/employees";
    const INTUIT_GUI_CUSTOMER           = self::INTUIT_GUI_URL . "app/customers";
    const INTUIT_GUI_ITEMS              = self::INTUIT_GUI_URL . "app/items";

    // the easiest way to access any entities created with the api
    const INTUIT_AUDIT_LOG              = self::INTUIT_GUI_URL . "app/auditlog";


	/////////////////////////////////////////////////////////////////////////////
    // OAUTH MANAGEMENT API
    //
    // general connection validation
    const CONNECTION_DEAD    = 0;
    const CONNECTION_ALIVE   = 1;
    const CONNECTION_STATUS  = self::CONNECTION_DEAD;

    // currently only Oauth1 works for this dev account
    const OAUTH1            = "oauth1";
    const OAUTH2            = "oauth2";
    const CONNECTION_TYPE   = self::OAUTH1; 

    // status types for connectivity assessment
    const OAUTH_DISCONNECTED    = 0;    // connection has no tokens
    const OAUTH_EXPIRED         = 1;    // connection tokens exist, but they are stale ( based on OAUTH_TOKEN_EXPIRATION)
    const OAUTH_VALID           = 2;    // connection tokens exist, and are within the expiry window, but have NOT been used for data retrieval in this session
    const OAUTH_ALIVE           = 3;    // the connection has tokens and has been confirmed
    // by default always assume there is no connection
    const OAUTH_STATUS          = self::OAUTH_DISCONNECTED;

    // OAUTH1
    const OAUTH1_TOKEN_EXPIRATION        = 180 * 24 * 60 * 60;   // according to API specs tokens are valid for ~ 180 days
    // collection of OAUTH1.0a endpoints referenced here:
    // https://developer.intuit.com/docs/00_quickbooks_online/2_build/10_authentication_and_authorization/40_oauth_1.0a/oauth_management_api
    const URL_REQUEST_TOKEN         = 'https://oauth.intuit.com/oauth/v1/get_request_token';
    const URL_ACCESS_TOKEN          = 'https://oauth.intuit.com/oauth/v1/get_access_token';
    const URL_CONNECT_BEGIN         = 'https://appcenter.intuit.com/Connect/Begin';
    const URL_APPCENTER_API         = 'https://appcenter.intuit.com/api/';
    const URL_CONNECT_DISCONNECT    = self::URL_APPCENTER_API . 'v1/Connection/Disconnect';
    const URL_CONNECT_RECONNECT     = self::URL_APPCENTER_API . 'v1/Connection/Reconnect';
    const URL_CURRENT_USER          = self::URL_APPCENTER_API . 'v1/user/current';
    const URL_APP_MENU              = self::URL_APPCENTER_API . 'v1/Account/AppMenu';

    // OAUTH2
    // different authentication flow requires different values. reference here:
    // https://developer.intuit.com/docs/00_quickbooks_online/2_build/10_authentication_and_authorization/10_oauth_2.0


	/////////////////////////////////////////////////////////////////////////////
    // Random other settings
    // 
    // TODO - where do I find that reference?
    const INTUIT_APIWIDGET_URL           = "https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js"; 
	const API_VERSION = 3;

	/**
	 * @param  boolean true when trying to hit the sandbox
	 * @return string that describes the base url for the quickbooks api
	 */
	public static function getApiBaseUrl($sandbox) {
		if ( $sandbox ) {
			return self::INTUIT_SANDBOX_API_URL;
		}
		return self::INTUIT_PRODUCTION_API_URL;
	}
    /**
     * @return integer indicating after how many seconds the access token expires
     */
    public static function tokenLife() {
        switch( self::CONNECTION_TYPE ) {
            case self::OAUTH1:
                return self::OAUTH1_TOKEN_EXPIRATION;        
            default:
                throw new CHttpException(500,'Invalid connection type specified in your QboConnection class.');
        }        
    }
    /**
     * @return array of selected keyed connection details to show during debugging
     */
    public static function getConnectionDetails( $debug = true ) {
 
        $details = array();
        $slugs = array( 
                    "grant"     => self::QBO_SLUG_OAUTH_GRANT,
                    "success"   => self::QBO_SLUG_OAUTH_SUCCESS,
                    "menu"      => self::QBO_SLUG_OAUTH_MENU,
                    "disconnect"=> self::QBO_SLUG_OAUTH_DISCONNECT, 
        );
        $localDev = array (
            "host_domain"   => self::QBO_LOCALDEV_HOST_DOMAIN,
            "launch_url"    => self::QBO_LOCALDEV_LAUNCH_URL,
            "disconnect"    => self::QBO_LOCALDEV_DISCONNECT_URL,
        );

        $sandbox = array(
            "api"   => self::INTUIT_SANDBOX_API_URL,
            "gui"   => self::INTUIT_SANDBOX_DASHBOARD,
            "box"   => self::INTUIT_SANDBOX_URL,
        );
    
        $production = array(
            "api"   => self::INTUIT_PRODUCTION_API_URL,
        );

        $authentication = array( 
            "api"           => self::URL_APPCENTER_API,
            "type"          => self::CONNECTION_TYPE,
            "expiry"        => self::OAUTH1_TOKEN_EXPIRATION,
            "requestoken"   => self::URL_REQUEST_TOKEN,
            "accesstoken"   => self::URL_ACCESS_TOKEN,
            "connect"       => self::URL_CONNECT_BEGIN,
            "reconnect"     => self::URL_CONNECT_RECONNECT,            
            "disconnect"    => self::URL_CONNECT_DISCONNECT,
            "user"          => self::URL_CURRENT_USER
        );
        // adjust as needed once rolling it to production
        $response = array(
            "slugs"             => $slugs,
            "localdev"          => $localDev,
            "sandbox"           => $sandbox,
            "authentication"    => $authentication
        );
        return $response;
    }
    /**
     * @return string 
     */
    public static function getAuthenticationTypeString() 
    {
        switch ( self::CONNECTION_TYPE ) {
            case self::OAUTH1:
                return "Oauth 1.0a";
            case self::OAUTH2:
                return "Oauth 2.0";
            default:
                return "Undefined";
        }
    }
    /**
     * @param  the quickbooks entity name that should be linked to
     * @return string of url where the entity LIST can be viewed (gui does not give direct access to individual entities)
     */
    public static function getIntuitGuiUrl( $entityName ) 
    {
        switch( strtolower($entityName )) {
            case "employee":
                return self::INTUIT_GUI_EMPLOYEE;
            case "customer":
                return self::INTUIT_GUI_CUSTOMER;
            case "item":
                return self::INTUIT_GUI_ITEMS;
            default:
                return false;
        }
    }
    /**
     * Throwing this in here during development via local vm. 
     * Slightly modified to suit our needs...
     * 
     * Check Internet Connection.
     * 
     * @author           Pierre-Henry Soria <ph7software@gmail.com>
     * @copyright        (c) 2012-2013, Pierre-Henry Soria. All Rights Reserved.
     * @param            string $sCheckHost Default: www.example.com
     * @return           boolean
     */
    public static function checkInternetConnection($sCheckHost = self::URL_APPCENTER_API) 
    {
        // apparently fsockopen only plays nice with the HOST part of a url
        $sCheckHost = parse_url($sCheckHost, PHP_URL_HOST);
        return (bool) @fsockopen($sCheckHost, 80, $iErrno, $sErrStr, 5);
    }
}