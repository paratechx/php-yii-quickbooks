

<?php
$this->breadcrumbs = array(
  'Quickbooks'  => array('/qbo'),
  'Timeactivity'   => array('/qbo/timeactivity'),
  Yii::t('app', 'Export'),
);
?>
<?php $this->renderPartial('/_ipp_button', array() ); ?>
<?php $this->renderPartial('/partial_views/_ajax_timeout', array() ); ?>
<?php $this->widget('Flasher'); ?>

<h1>Quickbook Timeactivity Export</h1>

<h4> </h4>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'timeactivity-export-form',
    'enableAjaxValidation'=>true,
)); ?>


<b>From :</b>
<?php 
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model'         => $model,
    'attribute'     =>'from_date',
    'options'=>array(
        'altFormat' =>'@', //send as timestamp
        'dateFormat' => 'yy-mm-dd',
        'showAnim'=>'fold',
        //'onSelect' => "js:function(selectedDate) {}",
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px; width:100px;',
        'class'=>'rounded-subtle'
    ),
));
?>

<b>To :</b>
<?php 
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model'         => $model,
    'attribute'     =>'to_date',
    'options'=>array(
        'altFormat' =>'@', //send as timestamp
        'dateFormat' => 'yy-mm-dd',
        'showAnim'=>'fold',
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px; width:100px;',
        'class'=>'rounded-subtle'
    ),
));
?>

<?php echo CHtml::submitButton('Update'); ?>
<?php echo CHtml::submitButton('Export',  array('onclick' => "return confirm('Exporting will remove all previous activity-exports for the selected date range. Continue??');", "name" => "postaction", "class"=>"", "style"=>"width:auto;")); ?>


<?php $this->endWidget(); ?>

<h3>


<br />
<br />
<h3> Review Data to be exported and correct any issues: </h3>
<h4> <strong> ** IMPORTANT: ** Quickbooks time exported is the daily total and not broken out into regular and overtime hours.</strong></h4> 
<br />



<?php 
//keep this in case we need specialized handling of the ajax window
// $widgetUpdate = 'js:function(data){
//     console.log("callback-data:" + data);
//     console.log( this );
//     return;
//   // hawki seems to stick with {status, content} objects, so assumption here is to catch logged out users
//   if(typeof data.redirect != "undefined" )
//   { 
//     if ( data.redirect == "YII_LOGIN_REQUIRED" ) {
//         // assume that our login has expired - reload our current page
//         window.location.reload();
//     }
//   }
//   else
//   {
//     if(typeof $.fn.yiiGridView != "undefined") {
//       // Update all gridviews
//       $( ".grid-view" ).each( function(){
//         $.fn.yiiGridView.update( $( this ).attr( "id" ) );
//       });
//       updateDialog.close();
//     }
//   }
// }';

$this->widget( 'ext.EUpdateDialog.EUpdateDialog', array(
  'dialogOptions'=>array('width'=>750,'minHeight'=>325),
  'options' => array(),
));

// Yii::app()->clientScript->registerScript('mysearch', "
//  $(document).delegate('.mysearchbtn', 'click', function(event){ 
//    $('tr.filters').toggle();
//    return false;
//  });
//  // $(document).delegate('.items', 'click', function(event){ 
//  //   if ( $(event.toElement).is('a') )
//  //     return true;
//  //   else 
//  //     return false;
//  // });
// ");

/** 
                    ct.id AS id,
                    CONCAT(u.lastname, u.firstname) AS user_name, 
                    DATE_FORMAT(FROM_UNIXTIME(starttime), '%Y-%b-%e') AS activity_date, 
                    p.name AS project_name, 
                    t.name AS task_name, 
                    SUM( ct.direct_mins ) AS activity_minutes, 
                    pmap.id AS remote_project_id, 
                    umap.id AS remote_user_id, 
                    umap.allow_export AS allow_export
 *  */
$debugColumns = array(
  // array(
  //   'header'        => 'CompanyTotals ID',
  //   'type'          => 'raw',
  //   'name'          => 'ctid',
  //   'value'         => '$data["ctid"]',
  // ),
  // array(
  //   'header'        => 'Local Project ID',
  //   'type'          => 'raw',
  //   'name'          => 'project_id',
  //   'value'         => '$data["project_id"]',
  // ),
);


$standardColumns = array(
  array(
    'header'        => 'Hawki User',
    'type'          => 'raw',
    'name'          => 'user_name',
    'value'         => '$data["user_name"]',
  ),
  array(
    'header'        => 'Quickbooks Employee',
    'type'          => 'raw',
    'name'          => 'user_name',
    'value'         => '$this->grid->controller->getMappedUser( $data )',
    //'htmlOptions'   => array('style' => ''),
  ),
  array(
    'header'  => 'Date',
    'type'    => 'raw',
    'name'    => 'activity_date',
    'value'   => '$data["activity_date"]',
  ),
  array(
    'header'  => 'Hawki Project',
    'type'    => 'raw',
    'name'    => 'project_name',
    'value'   => '$data["project_name"]',
  ),
  array(
    'header'  => 'Quickbooks Job',
    'type'    => 'raw',
    'name'    => 'project_name',
    'value'   => '$this->grid->controller->getMappedProject( $data );',
  ),  
  array(
    'header'  => 'Time',
    'type'    => 'raw',
    'name'    => 'activity_minutes',
    'value'   => '$this->grid->controller->getModel()->formatMinutes( $data["activity_minutes"] )',
  )
);

$listColumns = array_merge($standardColumns, $debugColumns); //, $entityColumns, $actionColumn );

$grid_id = "export-timeactivity-grid";
$pageSizer = '<div class="page-sizer summary"> Records per page: ' . 
             CHtml::dropDownList( 'pageSize', $pageSize, $this->getPageSizeOptions(), 
                                        array( 'onchange' => "$.fn.yiiGridView.update('$grid_id',{ data:{pageSize: $(this).val() }})", )
                    ) . '</div>';
?>

<?php 
$this->widget('ext.groupgridview.GroupGridView', 
  array(
    'id'              => $grid_id,
    'mergeColumns'    => array("user_name"),
    'dataProvider'    => $dataProvider1,
    'cssFile'         => false,
    'itemsCssClass'   =>'entity items',
    'ajaxUpdate'      => true,
    'afterAjaxUpdate' => 'function(id, data) { jQuery(".update-dialog-open-link" ).on( "click", updateDialogOpen ); }',  
    'ajaxUpdateError' => 'function(XHR, textStatus, errorThrown, err, id) { console.log("error"); console.log( "text: " + textStatus ); }',
    'filter'          => $model,
    'selectableRows'  => 0,
    'selectionChanged'=>'function(id){}',
    'columns'         => $listColumns, 
    'mergeCellCss'    => 'vertical-align:top;text-align:left;border-bottom:1px solid #ccc;',
    'template'        => "$pageSizer{summary}{items}{pager}",    
  )
);

// maybe implement in the future if time left or client requests
// $this->widget('ext.groupgridview.GroupGridView', 
//   array(
//     'id'              => "stale-$grid_id",
//     'dataProvider'    => $dataProvider2,
//     'ajaxUpdate'      => true,
//     'selectableRows'  => 0,
//     'selectionChanged'=>'function(id){}',
//     'columns'         => array('id', 'remote_entity_id', 'remote_entity_name'), 
//   )
// );

?>
