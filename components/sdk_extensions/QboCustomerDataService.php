<?php

use QuickBooksOnline\API\Data\IPPCustomer;

use QuickBooksOnline\API\Data\IPPReferenceType;
use QuickBooksOnline\API\Data\IPPPhysicalAddress;


/**
 *  Notes: 
 *      We only focus on Customer Entities that are a sub-entity of type "Job", as indicated by the Job-Parameter == true/1
 *
 * GOTCHAS:
 * 
 * The DisplayName attribute or at least one of Title, GivenName, MiddleName, FamilyName, or Suffix attributes is required during object create.
 *
 * AND the Job field has to be set to "1", instead of the more intuitive boolean "true"
 *
 *
 */

 /**
 * 
 * DataService wrapper class that exposes Customer entity specific functionality
 * 
 */
class QboCustomerDataService extends QboDataService {    
    /**
     * @return string used with query() call to retrieve entities of this type
     */
    protected function getFindAllQueryString() 
    {
        $query = "SELECT * FROM customer WHERE active=true AND job=true";
        return $query;
    }
    /**
     * @param  IPPCustomer
     * @return int
     */
    public function getEntityId( IPPCustomer $item ) 
    {
        return $item->Id;
    }
    /**
     * @param  IPPCustomer
     * @return string
     */
    public function getDisplayName( IPPCustomer $item ) 
    {
        return $item->FullyQualifiedName;
    }
    /**
     * populate a remote entity object from a local entity model
     * 
     * @param  Project $localEntity that contains the attributes to be created on the remote entity
     * @return IPPCustomer that will let the DataService generate a quickbooks entity
     */
    protected function mapLocalFieldsToRemoteEntity( Project $localEntity, $requiredParameters ) {
        $params = array();

        if ( $localEntity->name ) {
            $params["DisplayName"] = $localEntity->name;
        }
        $params["Level"]    = 1;      // we only support single-level at the moment (0 is for root customers)
        
        // go figure -- https://github.com/intuit/QuickBooks-V3-PHP-SDK/issues/71
        $params["Job"]      = 1;   // indicates to quickbooks that this is a sub-customer/job

        // the only required parameter
        if ( isset( $requiredParameters["customer_id"] ) ) {
            // rather than building it ourselves, query it and let the SDK do the construction work
            $params["ParentRef"] = $this->getCustomerReference( $requiredParameters["customer_id"], "ParentRef" );
        }
        if ( isset( $requiredParameters["currency_name"] ) ) {
            // rather than building it ourselves, query it and let the SDK do the construction work
            $params["CurrencyRef"] = $requiredParameters["currency_name"];
        }

        // turn the activeRecord object parameters into an IPPIntuitEntity   
        return new IPPCustomer( $params );
    }
    /**
     * just a 'dumb' mapper function -- all field integrity validation should happen in the controller
     * -- makes error processing easier
     * 
     * @param  integer $remote_entity_id the id that identifies the quickbook entity on their server
     * @param  Project $entityRecord Yii model 
     * @return Project filled with attribute values
     */
    protected function mapRemoteEntityToLocalRecord( $entity_id, Project $entityRecord ) {
        $entity = $this->Query("SELECT * FROM customer WHERE id='$entity_id' AND job=true AND active=true");
        $error = $this->getLastErrorMessage();

        // query response isn't all that great
        if ( ! is_null( $entity ) && count( $entity ) == 1 ) {
            $entity = array_shift( $entity );
            $attributes = array();

            if ( $entity->FullyQualifiedName ) {
                $attributes["name"] = $entity->FullyQualifiedName;
            } else if ( $entity->DisplayName ) {
                $attributes["name"] = $entity->DisplayName;
            }

            // just a shot in the dark, trying to get bits and pieces for the address details
            if ( $entity->BillAddr ) {
                $ippAddress = $entity->BillAddr;
            }
            else if ( $entity->ShipAddr ) {
                $ippAddress = $entity->ShipAddr;
            }
            // a week attempt to match required fields
            if ( $ippAddress ) {
                if ( $ippAddress->Line1 )
                    $attributes["address"] = $ippAddress->Line1;
                if ( $ippAddress->City )
                    $attributes["city"] = $ippAddress->City;
                if ( $ippAddress->CountrySubDivisionCode )
                    $attributes["province"] = $ippAddress->CountrySubDivisionCode;
            }

            if ( $entity->CurrencyRef )
                $attributes["country"] = $entity->CurrencyRef;

            $entityRecord->attributes = $attributes;
        }
        // return the populated object to the caller, let them deal with saving data and field validation
        return $entityRecord;
    }

    /**
     *  oddly enough setting job=false is used to filter customers that are not jobs or sub-customers...
     *  from api:
     *  "If true, this is a Job or sub-customer. If false or null, this is a top level customer, not a Job or sub-customer."
     * 
     * @return array of key->value pairs that the key encoded as customer_id:3_character_currency_code
     */
    public function getTopLevelCustomers() {
        $result = $this->Query("SELECT * FROM customer WHERE active=true AND job=false");
        $error  = $this->getLastErrorMessage();

        // TODO -- process errors
        $response = array();
        foreach( $result as $entity ) {
            // this causes trouble if not set and the home-currency is different then customer default currency
            $currencyRef = $entity->CurrencyRef;

            $response[ $entity->Id . ":$currencyRef" ] = $entity->FullyQualifiedName . " ($currencyRef)";
        }        
        return $response;
    }
}