<?php
$this->widget( 'ext.EUpdateDialog.EUpdateDialog', array(
  'dialogOptions'=>array('width'=>650,'minHeight'=>325),
));

// Yii::app()->clientScript->registerScript('mysearch', "
//  $(document).delegate('.mysearchbtn', 'click', function(event){ 
//    $('tr.filters').toggle();
//    return false;
//  });
//  // $(document).delegate('.items', 'click', function(event){ 
//  //   if ( $(event.toElement).is('a') )
//  //     return true;
//  //   else 
//  //     return false;
//  // });
// ");

// the basic info that is always the same for all entities
$standardColumns = array(
  array(
    'header'  => 'Status',
    'type'    => 'raw',
    'name'    => 'id',
    'filter'  => array('Mapped'=>Yii::t('app','Mapped'),'Unmapped'=>Yii::t('app','Unmapped')),
    'value'   => '$this->grid->controller->getMappingStatus( $data );'
  ),
  array(
    'header'  => "Hawki " . $this->getHawkiEntityName(),
    'type'    => 'raw',
    'name'    => 'local_entity_id',
    'value'   => '$this->grid->controller->getLocalEntityLink( $data["local_entity_id"] );'
  ),
  array(
    'header'  => "Quickbooks " . $this->getQuickbooksEntityName(),
    'type'    => 'raw',
    'name'    => 'remote_entity_id',
    'value'   => '$this->grid->controller->getQuickbooksEntityLink( $data["remote_entity_id"], $data["remote_display_name"] );'
  ),
);

// some specific columns for the entity if passed as parameter to the renderPartial()
$entityColumns = (isset( $entityColumns) && count( $entityColumns )) > 0 ? $entityColumns :  array();

// the map actions should be universal for all entities as well
$actionColumn = array(
  array(
    // 'header'    => '<button id="mysearchbtn" class="mysearchbtn" >Search</button>',
    'class'     => 'CButtonColumn',
    'template'  => '{map} {remap} {unmap} {create}',
    'buttons'   => array(
      "map"   => array(
        'label'     => 'Map',
        'visible'   => '$this->grid->controller->isUnmapped( $data );',
        'url'       => '$this->grid->controller->getMapActionLink( $data );',
        // 'imageUrl'  => '', // uncomment and insert url to show image instead          
        'click' => 'updateDialogOpen',
        'options' => array(
          'data-update-dialog-title' => Yii::t( 'app', 'Map Employee to User' ),
          'rel' => 'tooltip', 
          'data-toggle' => 'tooltip', 
          'title' => Yii::t('app', 'Map Entity')            
        ),          
      ),
      "remap" => array(
        'label'     => 'Map',
        'url'       => '$this->grid->controller->getMapActionLink( $data );',
        'imageUrl'  => '',
        'options'   => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Map Entity')),
        'click'     => NULL,
        'visible'   => 'false'
      ),
      "unmap" => array(
        'label'     => 'Unmap',
        'url'       => '$this->grid->controller->getUnmapActionLink( $data );',
        //'imageUrl'  => '',
        'options'   => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Unmap Entity')),
        'click'     => NULL, // needs something or null
        'visible'   => '!$this->grid->controller->isUnmapped( $data );'
      ),
      "create" => array(
        'label'     => 'Create',
        'visible'   => '$this->grid->controller->isUnmapped( $data );',
        'url'       => '$this->grid->controller->getCreateActionLink( $data );',
        //'imageUrl'  => '',
        'options'   => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => Yii::t('app', 'Create New Entity')),
        'click'     => NULL,          
      )
    )
  )
);

$listColumns = array_merge($standardColumns, $entityColumns, $actionColumn );

$grid_id = $this->getHawkiEntityName() . '-' . $this->getQuickbooksEntityName() . '-maps-grid';
$pageSizer = '<div class="page-sizer"> Records per page: ' . 
             CHtml::dropDownList( 'pageSize', $pageSize, $this->getPageSizeOptions(), 
                                        array( 'onchange' => "$.fn.yiiGridView.update('$grid_id',{ data:{pageSize: $(this).val() }})", )
                    ) . '</div>';

$this->widget('zii.widgets.grid.CGridView', 
  array(
    'id'              => $grid_id,
    'dataProvider'    => $dataProvider,
    'cssFile'         => false,
    'itemsCssClass'   =>'entity items',
    'ajaxUpdate'      => true,
    //'afterAjaxUpdate' =>'function() { $(".mysearchbtn").click(); }',  
    'filter'          => $filter,
    'selectableRows'  => 0,
    'selectionChanged'=>'function(id){}',
    'columns'         => $listColumns, 
    'template'        => "$pageSizer{summary}{items}{pager}",
  )
);

        // http://www.yiiframework.com/wiki/106/using-cbuttoncolumn-to-customize-buttons-in-cgridview/
        // 'buttonID' => array
        // (
        //     'label'=>'...',     //Text label of the button.
        //     'url'=>'...',       //A PHP expression for generating the URL of the button.
        //     'imageUrl'=>'...',  //Image URL of the button.
        //     'options'=>array(), //HTML options for the button tag.
        //     'click'=>'...',     //A JS function to be invoked when the button is clicked.
        //     'visible'=>'...',   //A PHP expression for determining whether the button is visible.
        // )
      

      // 'template'  => '{map}{remap}',
      // 'buttons'   => array(
      //   'map' => array( 'url' => '$this->grid->controller->getActionLinks( array("id" => $data["id"] ) )' ),
      //   'remap' => array('url' => '')
      // ),
    // use the hidden columns for debugging
    //
    // array(
    //   'header'  => 'ID',
    //   'type'    => 'raw',
    //   'name'    => 'id',
    // ),
    // array(
    //   'header'  => 'Company',
    //   'type'    => 'raw',
    //   'name'    => 'company_id',
    //   // 'value'   => '$data->getMappingStatus();'
    // ),
    // array(
    //   'header'  => 'User',
    //   'type'    => 'raw',
    //   'name'    => 'user_id',
    //   // 'value'   => '$data->getMappingStatus();'
    // ),
    // array(
    //   'header'  => 'Employee',
    //   'type'    => 'raw',
    //   'name'    => 'qb_employee_id',
    //   // 'value'   => '$data->getMappingStatus();'
    // ),

?>      