<?php
/**
 * using this as wrapper class to help turn off the yii autoloading which interacts poorly with the SDK
 *
 * @author OnCallSolutions
 * @copyright OnCallSolutions
 * @package  qbo yii-module
 * 
 */
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Data\IPPReferenceType;

class QboDataService extends DataService {
    /**
     * wrapper to bypass yii autoload
     * 
     * @param  IppTimeActivity object that must have at least a valid ID
     * @return null|Excepiton
     * parent @throws IdsException
     */
    protected function deleteById( $entity )
    {
        //@TODO implement check that the entity has valid parameters and is the correct type

        Yii::$enableIncludePath = false;

        return parent::Delete( $entity );

        Yii::$enableIncludePath = true;
    
    }
    /**
     * wrapper to bypass yii autoload 
     *
     * --- 
     * 
     * The Read request. You can either pass an object that contains the Id that you want to read, or
     * pass the Entity Name and the Id.
     * Before v4.0.0, it supports the read of CompanyInfo and Preferences.
     * After v4.0.0, it DOES NOT support read of CompanyInfo or Preferences. Please use getCompanyInfo() or getCompanyPreferences() method instead.
     * Only use this one to do READ request with ID.
     *
     * Developer has two ways to call the GET request. Take Invoice for an example:
     * 1) FindById($invoice);
     * or
     * 2) FindById("invoice", 1);
     *
     * @param object|String $entity Entity to Find, or the String Name of the Entity
     * @return IPPIntuitEntity Returns an entity of specified Id.
     * @throws IdsException
     */
    public function FindById($entity, $Id = null)
    {
        Yii::$enableIncludePath = false;

        return parent::FindById( $entity, $Id);

        Yii::$enableIncludePath = true;
    }
    /**
     * wrapper to bypass yii autoload
     *  
     * @param string $query Query to issue
     * @param string $pageNumber Starting page number
     * @param string $pageSize Page size
     * @return array Returns an array of entities fulfilling the query.     
     */
    public function Query($query, $pageNumber = 0, $pageSize = 500) 
    {
        Yii::$enableIncludePath = false;

        return parent::Query( $query, $pageNumber, $pageSize );

        Yii::$enableIncludePath = true;
    }
    /**
     * wrapper to bypass yii autoload
     * -- calls on the child class to provide entity specific query if desired
     * 
     * @param string name of entity to be queried
     * @param integer 
     * @param integer
     * @return array Returns an array of entities of specified type.
     */
    public function FindAll($entityName, $pageNumber = 0, $pageSize = 500) 
    {
        Yii::$enableIncludePath = false;

        // provide child classes with limited access without repeating the code all the time
        $query = $this->getFindAllQueryString();
        if ( $query ) {
            return parent::Query( $query, $pageNumber, $pageSize );
        } else { 
            // when no query specified use the standard query method to find it all
            return parent::FindAll( $entityName, $pageNumber, $pageSize );
        }

        Yii::$enableIncludePath = true;
    }
    /**
     * wrapper to bypass yii autoload
     *
     * ---
     * Creates an entity under the specified realm. The realm must be set in the context.
     *
     * @param IPPIntuitEntity $entity Entity to Create.
     * @return IPPIntuitEntity Returns the created version of the entity.
     */
    public function Add($entity) 
    {
        Yii::$enableIncludePath = false;

        return parent::Add( $entity );        

        Yii::$enableIncludePath = true;
    }
    /**
     * wrapper to bypass yii autoload
     * 
     * @return IPPCompanyInfo CompanyInformation
     */
    public function getCompanyInfo()
    {
        Yii::$enableIncludePath = false;

        return parent::getCompanyInfo();        

        Yii::$enableIncludePath = true;
    }  
    /** 
     * the Intuit FaultHandler seems *faulty* 
     * -- it does not parse the xml response correctly, so we are doing it for them
     * 
     * @return string $message indicating what went wrong if anything
     */
    public function getLastErrorMessage() 
    {
        $message = false;
        $error = $this->getLastError();
        // if we got an error, then try to decypher it
        if ( $error && get_class( $error ) == "QuickBooksOnline\API\Core\HttpClients\FaultHandler" ) {
            // SDK doesn't parse anything, so we do it here
            $error->parseResponse( $error->getResponseBody() );

            $message = "[" . $error->getIntuitErrorCode() . "] " . $error->getIntuitErrorDetail();
        }
        return $message;
    }  
    /** 
     * wrapper to add a quickbooks entity on their server 
     * while giving control of field definitions to the entity child-classes
     * 
     * @param [User/Task/Project] - Yii Task object
     * @param array $requiredParameters keyed values that are collected from user (not stored in the mapping table)
     * @return  boolean true if record has been added 
     *
     * @throws  no error messages but responses can be retrieved via dataService->getLastErrorMessage()
     * 
     */
    public function AddEntity( $localEntity, $requiredParameters = array() ) 
    {
        Yii::$enableIncludePath = false;

        // let the specific entity define how the fields are mapped
        $entity = $this->mapLocalFieldsToRemoteEntity( $localEntity, $requiredParameters );

        // then pass the object to the SDK to do the dirty work
        return parent::Add( $entity );

        Yii::$enableIncludePath = true;

    }
    /**
     * wrapper function to help populate the specific local entity fields 
     * 
     * @param  integer $remote_entity_id the id that identifies the quickbook entity on their server
     * @param  [User/Project/Task] $localEntity Yii model 
     * @return [User/Project/Task] $localEntity containing updated attribute values matching those of the remote entity 
     */
    public function getLocalEntity( $remote_entity_id, $localEntity ) 
    {
        Yii::$enableIncludePath = false;

        $localEntity = $this->mapRemoteEntityToLocalRecord( $remote_entity_id, $localEntity );

        Yii::$enableIncludePath = true;

        return $localEntity;
    }
    /**
     * IPPReferenceType object generator
     * 
     * @param  integer $reference_id that points to the quickbooks entity of $type
     * @param  string $name
     * @param  string $type
     * @return IPPReferenceType that is required to create entities and store them via API calls
     */
    public function getIPPReference( $reference_id, $name, $type ) 
    {
        return new IPPReferenceType( array( "value" => $reference_id, "type" => $type, "name" => $name ));
    }
    /**
     * @param  integer $account_id
     * @param  string $name the account type, eg ExpenseAccountRef/RevenueAccountRef
     * @return IPPReferenceType
     */
    public function getAccountReference( $account_id, $name ) 
    {
        return $this->getIPPReference( $account_id, $name, "Account" );
    }
    /**
     * @param  integer $account_id
     * @param  string $name the account type, eg ParentRef
     * @return IPPReferenceType
     */
    public function getCustomerReference( $account_id, $name ) 
    {
        return $this->getIPPReference( $account_id, $name, "Customer" );
    }

}