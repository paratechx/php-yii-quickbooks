<?php
/**
 * handles all logic related to the mapping between
 * QUICKBOOKS - employee and HAWKI - users entities
 * 
 */
class EmployeeController extends DefaultEntityController
{
    protected $model;
    protected $local_entity_name        = "user";
    protected $remote_entity_name   	= "employee";


    protected function processLocalEntityCreation( $newUser ) 
    {
    	$loggedInUser = Yii::app()->user;
    	// TODO validate that the user is logged in? Controller should already have caught it!
        if ( $newUser->email ) {
            $newUser->company_id      = $loggedInUser->companyId;
            $newUser->create_user_id  = $loggedInUser->id;
            $newUser->update_user_id  = $loggedInUser->id;
            // TODO -- validate that the user has enough details to be saved
            // TODO -- don't we need to set a password?
        } else {
        	$newUser->addError("email", "Remote User must have an valid email field set. " );
        }
        return $newUser;
    }
    /**
     * acces to a simple switch of the allow_export bit as indicated by the query parameters
     * 
     * @return employee/list view rendered with flash message indicating outcome of toggle action
     */
    public function actionExport() 
    {
        $afterActionRoute   = "/" . $this->getUniqueId() . "/list";
        $response           = false;

        // pull in the parameter
        $remote_id   		= intval( Yii::app()->getRequest()->getQuery('rid') );
        $new_export_status	= boolval( Yii::app()->getRequest()->getQuery('allow', false) );
        $is_ajax     = Yii::app()->getRequest()->isAjaxRequest;

        $model = $this->getModel(); 
        $criteria = $model->getEssentialCriteria();
        // TODO -- limit the setting of flags to mapped entities via additional criteria

        $model = $model->findByPk( $remote_id,  $criteria );
        if ( ! is_null( $model ) ) {
	        $model->toggleExportFlag( $new_export_status );
            $response = $model->save();
        } 
        if ( $response ) {
            Yii::app()->user->setFlash("success", ( ( $new_export_status ) ? "Enabled " : "Disabled ") . $model->getDisplayName() . " export.");
        } else {
            Yii::app()->user->setFlash("error", "Unable to access remote mapping with id " . $remote_id . ".");
        }
        $this->redirect( $afterActionRoute );
    }

    protected function getExportActionLink( $data ) {
        // create route as controller/action pair as required by createUrl()
        $route = $this->id . "/" . "export";
        // in order to set the export action we need a properly mapped entity
        if ( ! ( isset( $data["id"]) && isset( $data["local_entity_id"]) )) {
            return "";
        }
        $remote_record_id 		= intval($data["id"]);
        $new_export_status 		= ! boolval( $data["allow_export"] );

        $parameters = array();

        // now decide what gets attached 
        if ( $remote_record_id > 0 ) {
        	// identify who gets their bit flipped
            $parameters["rid"] = $remote_record_id;
            // and what the new state of it will be
	        $parameters["allow"] = $new_export_status;
        }        

        // let yii construct a useful url
        $url = $this->createUrl( $route, $parameters );

		$label = CHtml::encode( $new_export_status ? "Enable Export" : "Disable Export" );
        $link =  $this->createUrl( $route, $parameters );
        $altLabel = array("title"=>"Switch export flag.");

        return CHtml::link( $label, $link, $altLabel );
    }
}