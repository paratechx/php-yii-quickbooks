<?php
/**
 @var $this ConnectionController 
 @var $connection array 
**/

$this->breadcrumbs=array(
      Yii::t('app', 'Quickbooks'),
);
?>

<?php $this->renderPartial('/_ipp_button'); ?>
<?php $this->widget('Flasher'); ?>

<h1> Quickbooks Online - Connection Status</h1>
<p> </p>

<p>
    <p><strong>Settings</strong></p>
    <ul>
        <li> Connection Status: <strong><?php echo $connection->getStatusString() ?></strong></li>
        <li> Credentials Status: <strong><?php echo $connection->getAuthenticationStatusString() ?></strong></li>
        <li> Oauth Type: <strong><?php echo QboConnection::getAuthenticationTypeString() ?></strong></li>
        <li> Token issued on: <strong><?php echo date("d F Y H:i:s", $connection->getTokenIssueTimestamp()) ?></strong></li>
        <li> Expires: <strong><?php echo date("d F Y H:i:s", $connection->getTokenDeadlineTimestamp()) ?></strong></li>
    </ul>

    <p><strong>Static Credentials</strong></p>
    <ul>
        <li> Api Base: <strong><?php echo $connection->getApiBaseUrl() ?></strong></li>
        <li> App Token: <strong><?php echo $connection->getAppToken() ?></strong></li>
        <li> Consumer Key: <strong><?php echo $connection->getConsumerKey() ?></strong></li>
        <li> Consumer Secret: <strong><?php echo $connection->getConsumerSecret() ?></strong></li>

    </ul>
    <p><strong>Dynamic Credentials</strong></p>
    <ul>
        <li> Access Token: <strong><?php echo $connection->getAccessToken() ?></strong></li>
        <li> Access Token Secret: <strong><?php echo $connection->getAccessTokenSecret() ?></strong></li>
        <li> Realm ID <strong><?php echo $connection->getRealmId() ?></strong></li>
    </ul>

    <p><strong>Connected Account Details</strong></p>
    <ul>
        <li> Quickbooks Company: <strong><?php echo $connection->getCompanyName() ?></strong></li>
        <li> User Account Screen Name: <strong><?php echo $connection->getUserDetails() ?></strong></li>
    </ul>

    <strong>Available Actions:</strong>
    <ul>
        <li>Connect (issue new tokens): <ipp:connectToIntuit></ipp:connectToIntuit></li>
        <li>Reconnect (refresh existing tokens): <a href="<?php echo $connection->getReconnectUrl() ?>">Reconnect</a></li>
        <li>Disconnect (invalidate existing tokens): 
            <?php echo $this->renderPartial('/_ipp_logout', array("disconnect_url" => $connection->getDisconnectUrl(), "link_label" => "Disconnect Quickbooks Account")); ?>
        </li>
    </ul>

</p>