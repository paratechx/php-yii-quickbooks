DROP TABLE IF EXISTS `QboEmployee`;
CREATE TABLE `QboEmployee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL COMMENT 'CONSTRAINT FOREIGN KEY (company_id) REFERENCES company(id)',
  `local_entity_id` int(11) DEFAULT NULL COMMENT 'CONSTRAINT FOREIGN KEY (local_entity_id) REFERENCES user(id)',
  `remote_entity_id` int(11) NOT NULL,
  `remote_display_name` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) unsigned DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) unsigned DEFAULT NULL,  
  `allow_export` bit NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `QboCustomer`;
CREATE TABLE `QboCustomer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL COMMENT 'CONSTRAINT FOREIGN KEY (company_id) REFERENCES company(id)',
  `local_entity_id` int(11) DEFAULT NULL COMMENT 'CONSTRAINT FOREIGN KEY (local_entity_id) REFERENCES project(id)',
  `remote_entity_id` int(11) NOT NULL,
  `remote_display_name` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) unsigned DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `QboItem`;
CREATE TABLE `QboItem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL COMMENT 'CONSTRAINT FOREIGN KEY (company_id) REFERENCES company(id)',
  `local_entity_id` int(11) DEFAULT NULL COMMENT 'CONSTRAINT FOREIGN KEY (local_entity_id) REFERENCES task(id)',
  `remote_entity_id` int(11) NOT NULL,
  `remote_display_name` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) unsigned DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) unsigned DEFAULT NULL,  
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `QboTimeactivity`;
CREATE TABLE `QboTimeactivity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL COMMENT 'CONSTRAINT FOREIGN KEY (company_id) REFERENCES company(id)',
  `local_entity_id` int(11) DEFAULT NULL COMMENT 'CONSTRAINT FOREIGN KEY (local_entity_id) REFERENCES companytotals(id)',
  `user_id` int(11) DEFAULT NULL COMMENT 'CONSTRAINT FOREIGN KEY (user_id) REFERENCES user(id)',  
  `project_id` int(11) DEFAULT NULL COMMENT 'CONSTRAINT FOREIGN KEY (project_id) REFERENCES project(id)',  
  `activity_minutes` int(11) DEFAULT 0,
  `remote_entity_id` int(11) DEFAULT NULL,
  `remote_display_name` varchar(255) DEFAULT NULL,
  `activity_date` date NOT NULL,  
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) unsigned DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) unsigned DEFAULT NULL,  
  `active` bit NULL DEFAULT 0,
  `queued` bit NULL DEFAULT 0,
  `sync_token` int unsigned NULL,
  PRIMARY KEY (`id`)
);


DROP TABLE IF EXISTS `QboToken`;
CREATE TABLE `QboToken` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT 'CONSTRAINT FOREIGN KEY (user_id) REFERENCES user(id)',
  `company_id` int(11) unsigned NOT NULL COMMENT 'CONSTRAINT FOREIGN KEY (company_id) REFERENCES company(id)',
  `app_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token_secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realm_id` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request_datetime` datetime NOT NULL,
  `access_datetime` datetime DEFAULT NULL,
  `touch_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;