<?php

// use this class to define the data model for the qbo_token table
class QboToken extends JActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function init()
    {
        return parent::init();
    }
    public function tableName()
    {
        return 'qbo_token';
    }
    public function __toString() {
        return (string) $this->firstname . ' '. $this->lastname;
    }
    public function relations()
    {
        return array(
          'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
          'user'    => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }
    public function afterFind() {
        $val = parent::afterFind();

        $security = new QboSecurityManager();

        // Quickbooks requires those 3 to be encrypted with AES level stuff!
        $this->access_token         = $security->decrypt( $this->access_token );
        $this->access_token_secret  = $security->decrypt( $this->access_token_secret );
        $this->realm_id             = $security->decrypt( $this->realm_id );

        return $val;
    }
    protected function beforeSave()
    {
        $isNew = $this->isNewRecord;
        $val = parent::beforeSave();
        if ( $val ) {
            if ( ! isset( $this->company_id ) ) {
                $this->getCompanyId();
            }

            if ( Yii::app()->user ) {
                $user_id = Yii::app()->user->id;
                $this->user_id = $user_id;
            }
            $security = new QboSecurityManager();

            // Quickbooks requires those 3 to be encrypted with AES level stuff!
            $this->access_token = $security->encrypt( $this->access_token );
            $this->access_token_secret = $security->encrypt( $this->access_token_secret );
            $this->realm_id = $security->encrypt( $this->realm_id );

            if ( $isNew )
                $this->request_datetime = new CDbExpression('NOW()');
            $this->access_datetime = new CDbExpression('NOW()');
            $this->touch_datetime = new CDbExpression('NOW()');
        }
        return $val;
    }
    public function rules() {
        return array(
            array('user_id','safe'),
            array('company_id','safe'),
            array('app_token','safe'),
            array('access_token','safe'),
            array('access_token_secret','safe'),
            array('realm_id','safe'),
            array('source','safe'),
        );
    }
    // // return a dataProvider that points to token(?s) with the same company id
    // public function searchByCompany( $company_id = false) {
    //     if ( ! $company_id ) {
    //         $company_id = Yii::app()->user->companyId;  
    //     }

    //     $criteria = new CDbCriteria;
    //     if ( $company_id ) {
    //         $criteria->compare('`t`.company_id', Yii::app()->user->companyId );
    //     }
    //     // requires relation to be established in the relations()
    //     $criteria->with = array('company');

    //     return new CActiveDataProvider(get_class($this), array(
    //       'criteria'=>$criteria,
    //     ));  
    // }  
    // just a bundle of data so consumer doesn't have to remember the db fields
    public function setTokenData($app_token, $access_token, $access_secret, $realm_id, $source) {
        $this->setAttributes( array(
            "app_token"             => $app_token,
            "access_token"          => $access_token,
            "access_token_secret"   => $access_secret,
            "realm_id"              => $realm_id,
            "source"                => $source
        ));
       return $this;
    }
    /**
     * @param  integer $company_id identifies the company on hawki
     * @param  string $app_token identifies the hawki-quickbooks-online app
     * @return QboToken object
     */
    public function findCompany( $company_id, $app_token ) {

        $parameters = array( "company_id" => $company_id, "app_token" => $app_token, "source" => "QBO");

        $result = $this->findByAttributes( $parameters );

        return $result;
    }
    public function assignOwner( $user_id, $company_id ) {
        $this->setAttributes( array( 
            "user_id"       => $user_id,
            "company_id"    => $company_id ));
        return $this;
    }    
    public function getAppToken() {
         return $this->app_token;     
    }
    public function getToken() {
        return $this->access_token;
    }
    public function getTokenSecret() {
        return $this->access_token_secret;
    }   
    public function getRealmId() {
        return $this->realm_id;
    }
    public function getSource() {
        return $this->source;
    }
    /**
     * @return integer timestampe of when the token request was updated
     */
    public function getRequestTimestamp() 
    {
        return strtotime( $this->request_datetime );
    }
    /**
     * @param  integer value of the amount of seconds after which the access token expires
     * @return boolean indicating if the token is expired
     */
    public function isExpired( $seconds ) 
    {
        $expiryDate = $this->getExpiryDate( $seconds );

        if ( $expiryDate < time() ) {
            return true;
        }
        return false;
    }
    /**
     * @param  integer value of the amount of seconds after which the access token expires
     * @return date when the token approximately expires
     */
    public function getExpiryDate( $seconds ) 
    {
        $issueDate = $this->request_datetime;
        $expiryDate = strtotime( $issueDate ) + $seconds;

        return $expiryDate;
    }
    
    public function setTokenFields( $token, $secret )
    {
        $this->access_token = $token;
        $this->access_token_secret = $secret;
    }
    /**
     * update the token fields 
     * 
     * @param  [type]
     * @param  [type]
     * @return [type]
     */
    public function updateTokenFields( $token, $secret ) 
    {
        $this->setTokenFields( $token, $secret);
        return $this->save();
    }
    /**
     * @return integer that connects the user to a hawki company
     */
    public function getCompanyId() 
    {
        if ( ! isset( $this->company_id ) ) {
            $user = Yii::app()->user;
            if ( ! isset( $user->companyId ) ) {
                throw new CHttpException(500,'The user must have a valid company assigned!');
            }
            $this->company_id = $user->companyId;
        }
        return $this->company_id;
    }  
    /**
     * Stored in the token table and marked as "USER" in the source column
     *
     * add new ones by calling
     * /qbo/oauth/credentials?app_token=...&key=...&secret=...
     *
     * Locate dash here: https://developer.intuit.com/v2/ui#/app/dashboard
     * Select the correct app, ie: https://developer.intuit.com/v2/ui#/app/appdetail/b7sgjejvhv/b7sgjejvq7/dashboard
     *
     * Pull the credentials from the developer dashboard - Keys tab: 
     * oauth1 - app_token, consumer_key, consumer_secret
     * oauth2 - client_key, client_secret
     * 
     * @return QboToken object with the app credentials
     */
    public function getAppCredentials( $app_token ) 
    {
        $parameters = array( "app_token" => $app_token, "realm_id" => '', "source" => "USER" );
        $result = $this->findByAttributes( $parameters );

        if ( ! is_null( $result) ) 
            return $result;
        else 
            return null;
    }   
    public function getConsumerKey() {
        return $this->access_token;
    }
    public function getConsumerSecret() {
        return $this->access_token_secret;
    }
}