<?php
$this->breadcrumbs = array(
  'Quickbooks'  => array('/qbo'),
  'Timeactivity'   => array('/qbo/timeactivity'),
  Yii::t('app', 'Mapped List'),
);
?>
<?php $this->renderPartial('/_ipp_button', array() ); ?>
<?php $this->widget('Flasher'); ?>

<h1>Quickbook Timeactivity Export History</h1>

<?php
$grid_id = "timeactivity-history-grid";
$pageSizer = '<div class="page-sizer"> Records per page: ' . 
             CHtml::dropDownList( 'pageSize', $pageSize, $this->getPageSizeOptions(), 
                                        array( 'onchange' => "$.fn.yiiGridView.update('$grid_id',{ data:{pageSize: $(this).val() }})", )
                    ) . '</div>';

$this->widget('zii.widgets.grid.CGridView', array(
    'id'            => $grid_id,
    'dataProvider'  => $model->history(),
    'filter'        => $model,
    'columns'       => array(
        'id',
        'active',
        'queued',
        'activity_date',
        'remote_display_name',
        'create_time',
    ),
    'template'      => "$pageSizer{summary}{items}{pager}",
));
?>