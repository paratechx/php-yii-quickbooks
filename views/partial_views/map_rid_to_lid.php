<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'create-entity-mapping-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
    )); 
    ?>

    <?php $this->widget( 'ext.EChosen.EChosen', array('options' => array('allow_single_deselect' => true))); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->hiddenField($model,'local_entity_id'); ?>

    <div class="row">
    <?php       
        $listdata = CHtml::listdata($matchModels,'id','remote_display_name');

        if ( count( $listdata ) ) {
            echo $form->labelEx($model,'remote_entity_id');  
            echo $form->dropDownlist($model,'id',$listdata, array( 'class'=>'chzn-select',
                                                                   'style'=>'width:350px',
                                                                   'data-placeholder'=>'Select a match...')); 
        } else {
            echo "<p>No mappable data available. Use the [Create] button to generate an entity automatically.</p>" ;
        }
        echo $form->error($model,'remote_entity_id'); 
    ?>
    </div>

    <div class="row buttons">
    <?php
        echo CHtml::link( 'Cancel', $cancelRoute, array( 'class' => 'update-dialog-cancel-button buttonlook' ) );
        if ( count( $listdata ) )
            echo CHtml::submitButton( Yii::t('app', 'Save'), array("name" => "postaction", "class"=>"buttonlook", "style"=>"width:auto;")); 
        else 
            echo CHtml::link( 'Create', $createRoute, array( 'class' => 'update-dialog-create-button buttonlook' ) );
    ?>
    </div>

    <?php $this->endWidget(); ?>

</div>
