<div class="form">
    <h5> Please select the mandatory attributes that will be used to create a new entity in Quickbooks</h5>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'create-entity-attributes-selection-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
    )); 
    ?>

    <?php $this->widget( 'ext.EChosen.EChosen', array('options' => array('allow_single_deselect' => true))); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->hiddenField($model,'id'); ?>

    <div class="row">
    <?php 
        echo Chtml::label("Customer","customer_id", array());
        echo Chtml::dropDownList("customer_id",$defaultCustomer,$topLevelCustomers,
                                 array( 'class'=>'chzn-select',
                                        'style'=>'width:350px',
                                        'data-placeholder'=>'Select a match...'));
    ?>
    </div>

    <div class="row buttons">
    <?php
        echo CHtml::link( 'Cancel', $cancelRoute, array( 'class' => 'update-dialog-cancel-button buttonlook' ) );
        echo CHtml::submitButton( Yii::t('app', 'Create'), array("name" => "postaction", "class"=>"buttonlook", "style"=>"width:auto;")); 
    ?>
    </div>

    <?php $this->endWidget(); ?>

</div>

<?php 
/**

A reference to what this stuff is:

Create a new TOP-level customer via: https://sandbox.qbo.intuit.com/app/customers

The DisplayName attribute or at least one of Title, GivenName, MiddleName, FamilyName, or Suffix attributes is required during object create.

ParentRef:
optional
ReferenceType, default is null 
A reference to a Customer object that is the immediate parent of the Sub-Customer/Job in the hierarchical Customer:Job list. 
Required for the create operation if this object is a sub-customer or Job. Query the Customer name list resource to determine the appropriate Customer object for this reference. Use Customer.Id and Customer.DisplayName from that object for ParentRef.value and ParentRef.name, respectively.

 * 
 */
?>

