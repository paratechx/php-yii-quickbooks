<div class="form">
    <h5> Please select the mandatory attributes that will be used to create a new entity in Quickbooks</h5>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'create-entity-attributes-selection-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
    )); 
    ?>

    <?php $this->widget( 'ext.EChosen.EChosen', array('options' => array('allow_single_deselect' => true))); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->hiddenField($model,'id'); ?>

    <div class="row">
    <?php
        echo Chtml::label("Expense Accounts","expense_account_id", array());   
        echo Chtml::dropDownList("expense_account_id",$defaultExpenseAccount,$expenseAccounts, 
                                 array( 'class'=>'chzn-select',
                                        'style'=>'width:350px',
                                        'data-placeholder'=>'Select a match...'));
    ?>
    </div>
    <div class="row">
    <?php 
        echo Chtml::label("Income Accounts","income_account_id", array());
        echo Chtml::dropDownList("income_account_id",$defaultIncomeAccount,$incomeAccounts, 
                                 array( 'class'=>'chzn-select',
                                        'style'=>'width:350px',
                                        'data-placeholder'=>'Select a match...'));
    ?>
    </div>

    <div class="row buttons">
    <?php
        echo CHtml::link( 'Cancel', $cancelRoute, array( 'class' => 'update-dialog-cancel-button buttonlook' ) );
        echo CHtml::submitButton( Yii::t('app', 'Create'), array("name" => "postaction", "class"=>"buttonlook", "style"=>"width:auto;")); 
    ?>
    </div>

    <?php $this->endWidget(); ?>

</div>

<?php 
/**

A reference to what this stuff is:


IncomeAccountRef:

required for Inventory and Service item types

ReferenceType 

Reference to the posting account, that is, the account that records the proceeds from the sale of this item. Must be an account with account type of 
Sales of Product Income
Classification--Revenue
. span>. Query the Account name list resource to determine the appropriate Account object for this reference. Use Account.Id and Account.Name from that object for IncomeAccountRef.value and IncomeAccountRef.name, respectively. 


ExpenseAccountRef

ExpenseAccountRef:
required for Inventory, NonInventory, and Service item types
ReferenceType 
Reference to the expense account used to pay the vendor for this item. Must be an account with account type of 
Cost of Goods Sold
Classification--Expense


. Query the Account name list resource to determine the appropriate Account object for this reference. Use Account.Id and Account.Name from that object for ExpenseAccountRef.value and ExpenseAccountRef.name, respectively. 


To get those Account References, we utilize the classification in the query() call. 
Classification='Expense' for Expenses
Classification='Revenue' for Income
 
Classification:
optional
String, filterable, default is derived from AccountType and AccountSubtype 
The classification of an account. Not supported for non-posting accounts. 
Valid values include: Asset, Equity, Expense, Liability, Revenue

**/

?>

