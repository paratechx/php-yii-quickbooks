<div style="text-align: center; font-family: sans-serif; font-weight: bold;">
	SUCCESS. CONNECTION ESTABLISHED! Please wait...
</div>
		
		
<script type="text/javascript">
	
    if( ! window.opener ) {
        // Top level window
       self.location = "<?php echo $this->createAbsoluteUrl('/qbo/') ?>";
    } else {
        // Not top level. An iframe, popup or something
        console.log( "Closing popup window. ")
        window.opener.location.reload(false);
        window.setTimeout('window.close();', 1000);
    }
</script>