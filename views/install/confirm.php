<div id="installer" class="confirm">

	<h2><?php echo Yii::t('install', 'Install Quickbooks Online Module'); ?></h2>

	<p class="red-text">
		<?php echo Yii::t('install', 'Qbo Module is already installed!'); ?>
	</p>

	<p><?php echo Yii::t('install', 'Please confirm if you wish to reinstall.'); ?></p>

	<p>
		<?php echo CHtml::link(Yii::t('install', 'Yes'), array('install/run', 'confirm'=>1)); ?> /
		<?php echo CHtml::link(Yii::t('install', 'No'), Yii::app()->homeUrl); ?>
	</p>

	<p class="info"><?php echo Yii::t('install', 'Notice: All your mapped quickbooks connections and data will be lost.'); ?></p>

</div>