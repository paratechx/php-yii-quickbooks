<?php
    // a more graceful way to deal with the useless 200 responses without messing with the actual hoursportal application setup
    // modified from
    // http://www.yiiframework.com/wiki/321/using-loginrequiredajaxresponse-to-solve-ajax-session-timeout/
    if ( Yii::app()->components['user']->loginRequiredAjaxResponse ) {
        $responseValue = Yii::app()->components['user']->loginRequiredAjaxResponse;
        Yii::app()->clientScript->registerScript('ajaxLoginRequired', "
            // intercept the 200 responses that just show $responseValue and do the actual login redirect
            // primarily used to prevent blank grid views after login times out
            jQuery(document).ajaxSuccess(
                function(event, request, options) {
                    // console.log( options );
                    // console.log( request );
                    try {
                        parsedJson = JSON.parse( request.responseText );
                    } catch(e) { 
                        parsedJson = {};
                    } 
                    // yii may send it as ajax, or object, or string in responseText, so handle all options...
                    if ( ( request.responseText == '$responseValue') ||
                         ( typeof parsedJson.redirect != 'undefined' && parsedJson.redirect == '$responseValue' ) || 
                         ( typeof request.responseJSON != 'undefined' && typeof request.responseJSON.redirect != 'undefined' && request.responseJSON.redirect == '$responseValue' ) ) 
                    {
                        // qbo view calls dont specify ajax parameter
                        if ( options.url.indexOf('ajax=') == -1 ) {
                            window.location.reload();
                        } else { // but the grids do, so we reload their request if we can
                            window.location.reload(); //simplified for now because the url gets ugly //window.location.href = options.url;
                        }
                    }
                }
            );
        ");
    }
?>