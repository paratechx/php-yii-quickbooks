
<h3> This list contains all mapped and unmapped hawki-users and quickbook-employees. To refresh the listing use the controls below:</h3>

 

<?php

Yii::app()->clientScript->registerScript('mysearch', "
  $(document).delegate('#mysearchbtn', 'click', function(){
    var htmlStr = $(this).html(); 
    if(htmlStr == 'Search')
    {
      $('tr.filters').show();
      $(this).html('Search');
    }
    else
    {
      //$('#fullname').val('');
      //$('#fstatus').val('0');
      $('tr.filters').hide();
      $(this).html('Search');
    }
    return false;
  });
");

?>
<?php
  $this->widget('application.widgets.EPageSize', array(
    'gridViewId' => 'employee-grid',
    'beforeLabel'=> 'Employees per page',
    'pageSize' => Yii::app()->request->getParam('pageSize',null),
    'defaultPageSize' =>  25 ,   // may use this :  Yii::app()->params['defaultPageSize'],
    'pageSizeOptions'=> array(5=>5, 10=>10, 25=>25, 50=>50, 75=>75, 100=>100), // you can config it in main.php under the config dir . Yii::app()->params['pageSizeOptions'],// Optional, you can use with the widget default
  ));
?>
<?php
  $pageSize = Yii::app()->user->getState('pageSize',25/*Yii::app()->params['defaultPageSize']*/);
  $mappedDataProvider->getPagination()->setPageSize($pageSize);
?>
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
  'id'=>'employee-grid',
  'dataProvider'=>$mappedDataProvider,
  'cssFile'=>false,
  'filter'=>$filter,
  'ajaxUpdate'=>false,
  'afterAjaxUpdate'=>'function() { $(".mysearchbtn").click(); }',
  'columns'=>array(
    array(
      'header'  => 'Status',
      'type'    => 'raw',
      'name'    => 'user_id',
      'value'   => '$data->getMappingStatus();'
    ),
    array(
      'header'  => 'Action',
      'type'    => 'raw',
      'name'    => 'user_id',
      'value'   => '$data->getAvailableActionLinks();'
    ),    
    array(
      'header'  => 'Mapped Local User',
      'type'    => 'raw',
      'name'    => 'user_id',
      'value'   => '$data->getMappedUserLink();'
    ),
    array(            
      // 'class'=>'CLinkColumn',
      'header'=>'Quickbooks Employee',
      'filter' => '<input id="displayname" type="text" name="QboEmployee[displayname]" value="'.$filter->remote_display_name.'"</input>',
      'htmlOptions'=>array('class' => 'employeename' ),
      'type'=>'raw',
      'name'=>'displayname',
      'value' => 'CHtml::encode($data->remote_display_name . " (" . $data->qb_employee_id . ")" )',
    ),
  )
));
?>
