<?php
/**
 * similar to the other entity mappers, except it only maps one way!
 * 
 */
class TimeactivityController extends DefaultEntityController
{
    protected $model;
    protected $local_entity_name        = "totalbla";
    protected $remote_entity_name       = "timeactivity";

    public function actionDev() {
        // insert your dev questions here
    }

    public function actionList() {        
        $model = new QboTimeactivity();

        $this->adjustPageSize();

        // attach a list of all available entities to the view
        $this->render('list', array(
            'model'     => $model,
            'pageSize'  => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
        ));
    }
    protected function beforeAction($action){
        // if(defined('YII_DEBUG') && YII_DEBUG){
            Yii::app()->assetManager->forceCopy = true;
        // }
        return parent::beforeAction($action);
    }
    public function actionSync() {
        $request = Yii::app()->getRequest();

        $queuedRecords = $this->getModel()->getActiveQueue();

        if ( count( $queuedRecords ) ) {
            $result = $this->_sendTimeactivity( $queuedRecords );

            $remaining = count($result["success"]) - count( $queuedRecords );

            Yii::app()->user->setFlash("success", "Submitted " . count( $queuedRecords ) . " timeactivity entries." );
            $this->redirect('list');
        } else {
            Yii::app()->user->setFlash("notice", "No active queue items found. Review the export history for details." );
            $this->redirect('list');
        }
    }
    public function actionExport() {
        $postaction = strtolower( Yii::app()->getRequest()->getPost("postaction"));
        $model = $this->getModel("export");        
        $user = Yii::app()->user; 

        $this->adjustPageSize();

        if ( isset($_POST['QboTimeactivity']) ) {
            $user->setState("QboTimeactivity", $_POST['QboTimeactivity']);
        }
        // these should contain only export parameters such as dates
        $model->attributes = $user->getState("QboTimeactivity", array() );

        $exportDataProvider = $model->getExportData();

        $staleData = $this->getModel()->findAllStaleActivities();
        $staleDataProvider = new CArrayDataProvider($staleData);

        if ( ! $exportDataProvider->hasUnmappedData() ) {
            // start processing if the export button has been hit
            if ( $postaction == "export") {                
                // set local mapping active-column to false and provide us with the remote ids
                $deactivatedRemoteIds = $this->_deactivateStaleRecords();  
                // if any unlinks happened, 
                if ( $deactivatedRemoteIds ) 
                    $deleted_stale_count = $this->_deleteStaleRemotes( $deactivatedRemoteIds );

                $queuedResult = $this->_queueTimeactivity( $exportDataProvider );
                $queuedCount = count( $queuedResult );

                if ( $queuedCount > 0 ) {
                    $msg = "Queued  $queuedCount new time records.";
                    $status = "success";
                } else {
                    $msg = "Nothing queued.";
                    $status = "notice";
                }
                Yii::app()->user->setFlash( $status, $msg );
                $this->redirect('sync');                
            } else {
                Yii::app()->user->setFlash("notice", "Please use the export button to process the selected date range." );
            }
        } else {
            Yii::app()->user->setFlash("error", "Unmapped data found in your export selection. </br> Please FIX them OR modify the selected DATES before trying to export." );
        }
        // attach a list of all available entities to the view
        $this->render('export', array(
            'model'         => $model,
            'dataProvider1' => $exportDataProvider,
            'dataProvider2' => $staleDataProvider,
            'pageSize'      => $user->getState('pageSize', Yii::app()->params['defaultPageSize']),            
        ));
    }
    protected function getMappedEntityLink($entity_table_name, $entity_id, $local_id, $mapped_name )
    {
        if ( ! $entity_id ) {
            $label = CHtml::encode( "Unmapped. Click to fix!" );
            $linkSegements = array("/qbo/$entity_table_name/map/", "lid" => $local_id );
            $htmlOptions = array("title"=>"", "class"=>"update-dialog-open-link");
            return CHtml::link( $label, $linkSegements, $htmlOptions );
        } else {
            return CHtml::encode( $mapped_name );
        }
    }

    public function getMappedProject( $data ) 
    {
        return $this->getMappedEntityLink( "customer", $data["remote_project_map_id"], $data["project_id"], $data["remote_project_display_name"] );
    }
    public function getMappedUser( $data )
    {
        return $this->getMappedEntityLink( "employee", $data["remote_user_map_id"], $data["employee_id"], $data["remote_user_display_name"] );
    }
    /**
     * because we dont know what the user does on the quickbooks side of things
     * client requested a one-way sync. to avoid duplicate days and entries, we inactivate all 
     * previous records for that date-range and criteria match
     * 
     * @return array containing the remote_entity ids for reference
     */
    protected function _deactivateStaleRecords() 
    {
        $unlinked = array();
        $model = $this->getModel();
        $result = $model->findAllStaleActivities();
        if ( count( $result ) ) {
            foreach( $result as $record ) {
                // call quickbooks and set their record to unlinked
                $record->active = 0;
                if ( $record->validate() ) {
                    if ( $record->save() )
                        $unlinked[] = $record->remote_entity_id;
                } else {
                    // TODO - throw error ??
                }
            }
        }
        return $unlinked;
    }
    private function _deleteStaleRemotes( $remoteIds )
    {
        if ( count( $remoteIds ) > 0 ) {
            $success_count = 0; 

            // remove old data as inactive
            $dataService    = $this->getDataService();

            foreach( $remoteIds as $key => $rid ) {
            // todo -- optimize with batch functionality
                $response = $dataService->deleteById( $rid );
                if ( ! $dataService->getLastErrorMessage() ) {
                    $success_count++;
                    unset( $remoteIds[$key] );
                }

            }
            // return $remoteIds; // if needed for debugging...
            return $success_count;
        } else {
            return false;
        }
    }
    /**
     * due to potential for large number of record submissions, we split the process into 
     * queue and sync functions. If users start hitting the throttle limits, we can modify
     * process to batch submissions with timed throttles.
     *  
     * @param  array $dataProvider data pulled from QboTimeactivity->getExportData()
     * @return array               log of ids for records that have been queued
     */
    private function _queueTimeactivity( $dataProvider )
    {
        $data           = $dataProvider->getData();
        $queuedIds      = array();

        if ( count( $data ) ) {
            foreach( $data as $record ) {
                $remoteRecord = $this->getModel(true);
                // set various attributes to prepare for submission to quickbooks
                $remoteRecord->queued           = true;                                   
                $remoteRecord->active           = true;
                $remoteRecord->activity_date    = $record["activity_date"];
                $remoteRecord->activity_minutes = $record["direct_mins"];
                $remoteRecord->user_id          = $record["employee_id"];
                $remoteRecord->project_id       = $record["project_id"];
                $remoteRecord->local_entity_id  = $record["ctid"]; // keep in mind that this ctid is not very useful due to the aggregate nature of the query!
                
                // just some visual references for what we have stored
                $employee_name = $record["user_name"];
                $project_name = $record["project_name"];
                $remoteRecord->setDisplayName( "[Employee:" . $employee_name . ", Project: " . $project_name . "]");

                // store last check time, and potential new record
                if ( $remoteRecord->validate() && $remoteRecord->save() ) {
                        $queuedIds[] = $remoteRecord->id;
                } else {
                    // TODO -- utilize getErrors() to give feedback?
                     throw new CHttpException(500,'Unable to record quickbook employee data in the database.');
                }
            }
        } else {
            throw new CHttpException(500,'Check your dates, the dataProvider could not find anything.');
        }
        return $queuedIds;
    }

    private function _sendTimeactivity( $data ) 
    {
        // remove mark old data as inactive
        $dataService    = $this->getDataService();
        $successIds     = array();
        $problemIds     = array();

        if ( count( $data ) ) {
            foreach( $data as $remoteRecord ) {
                // attempt to store it on quickbooks server
                $remoteEntity = $dataService->AddEntity( $remoteRecord );
                $error = $dataService->getLastErrorMessage();

                // now store employees details in our employee table, as we don't want to play name match games later
                if ( ! $error && ! is_null( $remoteEntity ) ) {
                    $remoteRecord->remote_entity_id = $remoteEntity->Id;
                    $remoteRecord->queued = false;
                } else {
                    $problemIds[] = $remoteRecord->id;
                }

                // store last check time, and potential new record
                if ( $remoteRecord->validate() && $remoteRecord->save() ) {
                    $successIds[] = $remoteRecord->id;
                } else {
                     throw new CHttpException(500,'Unable to record quickbook employee data in the database.');
                }
            }
        } else {
            throw new CHttpException(500,'Check your dates, the dataProvider could not find anything.');
        }
        $responseData = array( "success" => $successIds, "problems" => $problemIds );
        return $responseData;
    }
}