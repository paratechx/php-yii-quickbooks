<?php
/** 
 * model that builds the entity connection between 
 * QUICKBOOKS-Employees and HAWKI-Users
 *
 * it is a JActiveRecord child with some special sauce from the QboEntityMap parent
 */
class QboEmployee extends QboEntityMap
{
    protected $entity_table_name    = "user";
    protected $tableName            = "qbo_employee_to_user_map";

    // TODO -- cleanup this temporary override
    protected $entityMapColumns     = array("id", "local_entity_id", "company_id", "remote_entity_id", "remote_display_name", "allow_export");
    protected $entityColumns        = array( NULL, "id", "company_id", NULL, NULL, NULL); // must match the count and type (or NULL) of the entityMapColumns

    /**
     * @return array customizing the field labels to this particular entity
     */
    public function attributeLabels() 
    {
        return array(
            'id'                     => Yii::t('app', 'ID'),
            'local_entity_id'        => Yii::t('app', 'Mapped User'),
            'company_id'             => Yii::t('app', 'Company'),
            'remote_entity_id'       => Yii::t('app', 'Quickbooks Employee'),
            'remote_display_name'    => Yii::t('app', 'Quickbooks Name'),
            'create_time'            => Yii::t('app', 'Create Time'),
            'create_user_id'         => Yii::t('app', 'Create User'),
            'update_time'            => Yii::t('app', 'Update Time'),
            'update_user_id'         => Yii::t('app', 'Update User'),
            'allow_export'                 => Yii::t('app', 'Allow Export'),
        );
    }
    /** 
     *  must be in here, else breaks functionality...??
     * 
     * @param string of class name
     * @return static active record model instance.
     */
    public static function model($className=__CLASS__) 
    {
        return parent::model($className);
    }
    /**
     * only the User/Employee table has this field
     * 
     * @return boolean to progress the save() chain
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            // $this->allow_export = false;
            return true;
        } else {
            return false;
        }
    }

    public function toggleExportFlag( $new_status = false )
    {
        return $this->allow_export = $new_status;
    }
}
