<h4> <strong> Available Entity Controls </strong> </h4>
<ul>
    <li><h6> <?php echo CHtml::link( "Refresh Quickbooks Data", $this->getRefreshRoute() ); ?></h6></li>
<?php /**

With great power comes great responsibility...not exposing this for now as it has potential for a large field of errors...

    <li><h6> Create quickbook entities from unmapped hawki records </h6></li>
    <li><h6> Map quickbook entities to hawki records </h6></li>
    <li><h6> Clear all mappings</h6></li>
    **/ ?>
</ul>
