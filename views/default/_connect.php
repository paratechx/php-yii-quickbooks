<div style="border: 2px solid red; text-align: center; padding: 8px; color: red;">
	<b>NOT</b> CONNECTED!<br>
	<br />
	New Window:
	<ipp:connectToIntuit></ipp:connectToIntuit>
	<br />
	Same Window:
	<a href="<?php echo $this->createAbsoluteUrl('/qbo/oauth/connect') ?>"> Connect To Intuit </a>

	<br>
	<br>
	You must authenticate to QuickBooks <b>once</b> before you can exchange data with it. <br>
	<br>
	<strong>You only have to do this once!</strong> <br><br>

	After you've authenticated once, you never have to go
	through this connection process again. <br>
	Click the button above to
	authenticate and connect.
</div>

