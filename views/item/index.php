<?php
$this->breadcrumbs = array(
  'Quickbooks'  => array('/qbo'),
  Yii::t('app', 'Item Controls'),
);
?>

<?php $this->renderPartial('/_ipp_button'); ?>
<?php $this->widget('Flasher'); ?>

<h1>Quickbook Entity Controls - Service Items</h1>

    <?php 
    /** intuit does not appear to require it, just leaving it here in case things change:
        $this->renderPartial('/_ipp_button', array() ); 
    **/
    ?>

    <p> What would you like to do? </p>
    <p>
        <ul>
            <li><?php echo CHtml::link('Display Mapping List of Hawki-Tasks to Quickbooks Service Items', $this->createAbsoluteUrl('/qbo/item/list')); ?></li>
        </ul>
    </p>