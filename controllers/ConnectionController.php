<?php
/**
 * expose any connection related details / actions to the user through this class
 */
class ConnectionController extends DefaultController
{
    /**
     * @return views/connection/status.php containing the connection status details for the current user
     */
    public function actionIndex()
    {
        // prevents redirect when disconnected
        $this->authentication_redirect = false;

        $this->initIntuit();

        $this->render('index', array( 
            "connection" => $this->getIntuitObject()                // the main connection object 
        ));
    }
}
