<?php
/**
 * QBO Module class file.
 *
 * @author OnCallSolutions
 * @copyright OnCallSolutions
 * @package  qbo yii-module
 *
 */
class QboModule extends CWebModule
{
    public $install = false;

    public $debug = true;

    public $defaultController = 'default';

    public $app_token = '';

    private $_authorizer = false;

    private $_assets_url;

    /**
     * @property string the style sheet file to use for Payroll.
     */
    public $cssFile;

    private $_Intuit = false;

    /**
     * setting up the basic files and components
     *
     * @return moves on to controller action
     */
    public function init()
    {
        // import the module-level models and components
        $this->setImport(array(
            'qbo.models.*',
            'qbo.components.*',
            'qbo.components.sdk_extensions.*',
            'qbo.controllers.*',
        ));
        // this will call the external library setup
        $this->setComponent('installer', new QboInstaller);
        // an easy way for us to keep the menu code within this module
        $this->setComponent('menu', new QboMenu);
        // the main connection object for quickbooks
        $this->setComponent('intuit', new QboIntuit());
        // add a few more tools to the module code
        $this->registerScripts();
        // only used during dev to modify tables
        // $this->runExtras();
        // disable install mode via module settings
        if (!$this->getInstaller()->isInstalled()) {
            // attempt to install if the module setting flag is enabled
            if ($this->install) {
                $this->defaultController = 'install';
            } else {
                throw new CHttpException(412, 'Qbo module not installed. Enable the install flag via module configuration to start the install process.');
            }
        }
    }

    private function runExtras()
    {
        $installer = $this->getInstaller();
        // see the installer component for which models get refreshed
        $installer->refreshSelectedTables();
        if (count($installer->getRefreshLog())) {
            echo "Updated the folling model tables:" . implode(", ", $installer->getRefreshLog()) . "</br>";
        }
    }

    /**
     * Registers the necessary scripts.
     */
    public function registerScripts()
    {
        // Get the url to the module assets
        $assetsUrl = $this->getAssetsUrl();

        // Register the necessary scripts
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerCoreScript('jquery.ui');

        // Make sure we want to register a style sheet.
        if ($this->cssFile !== false) {
            // Default style sheet is used unless one is provided.
            if ($this->cssFile === null) {
                $this->cssFile = $assetsUrl . '/css/default.css';
            } else {
                $this->cssFile = Yii::app()->request->baseUrl . $this->cssFile;
            }

            // Register the style sheet
            $cs->registerCssFile($this->cssFile);
        }
    }
    /**
     * Publishes the module assets path.
     *
     * @return string the base URL that contains all published asset files of Qbo.
     */
    public function getAssetsUrl()
    {
        if ($this->_assets_url === null) {
            $assetsPath = Yii::getPathOfAlias('qbo.assets');
            // We need to republish the assets if debug mode is enabled.
            if ($this->debug === true) {
                $this->_assets_url = Yii::app()->getAssetManager()->publish($assetsPath, false, -1, true);
            } else {
                $this->_assets_url = Yii::app()->getAssetManager()->publish($assetsPath);
            }

        }
        return $this->_assets_url;
    }
    /**
     *
     * @return QboInstaller the installer component.
     */
    public function getInstaller()
    {
        return $this->getComponent('installer');
    }
    /**
     * this function creates the Intuit object and establishes a connection status
     *
     * @return QboIntuit holding all the connection details for Quickbooks Api
     */
    public function getIntuit()
    {
        if (!$this->_Intuit) {
            // this is how Yii seems to get objects, so sticking with it for now
            $Intuit = $this->getComponent('intuit');

            if (!is_null($this->app_token)) {
                // defined in the module settings
                $credentials = QboToken::model()->getAppCredentials($this->app_token);
            }

            if (is_null($credentials)) {
                throw new CHttpException(500, 'Qbo module missing the app_token. Contact your developer to setup the module.');
            } else {
                $credentials = array(
                    'app_token' => $credentials->getAppToken(),
                    'consumer_key' => $credentials->getConsumerKey(),
                    'consumer_secret' => $credentials->getConsumerSecret(),
                );
            }
            $Intuit->setCredentials($credentials);
            // split this for easier reading of code and ability to test connection explicitely
            $Intuit->connect();
            // assign it so every controller can grab as needed
            $this->_Intuit = $Intuit;
        }
        return $this->_Intuit;
    }
}
