<?php

/***
**  
**  manage connected apps here: https://appcenter.intuit.com/
**
** The following actions are available:
** 	/qbo/oauth 				-- index action that start the authentication via oauth if no active token exists
** 	/qbo/oauth/menu 		-- this is a way to render the code necessary for displaying the intuit blue-dot-menu
**	/qbo/oauth/success 		-- this is called when a connection is successfully established
** 	/qbo/oauth/disconnect 	-- invalidates any connection on behalf of the tenant
** 	/qbo/oauth/reconnect 	-- TODO --
**
**/

class OauthController extends DefaultController
{
	public $disconnectedRedirectUrl;
	private $_config;
	private $_accessControlRules;

	public function actionCredentials() 
	{
		$request 			= Yii::app()->getRequest();
        $app_token   		= $request->getQuery('app_token');
        $consumer_key   	= $request->getQuery('key');
        $consumer_secret   	= $request->getQuery('secret');
		$afterActionRoute 	= "/qbo/";

		// TODO -- only allow administrators to call this action?
		// TODO -- validate the input a bit more 
		if ( $app_token && $consumer_key && $consumer_secret ) {
	        $response = $this->setAppCredentials( $app_token, $consumer_key, $consumer_secret );
		} else 
	    	$response = false;

        if ( $response ) {
            Yii::app()->user->setFlash("success", "Stored credentials for $app_token.");
        } else {
            Yii::app()->user->setFlash("error", "Failed to store credentials.");
        }
        $this->redirect( $afterActionRoute );
	}
	// used for the auth sequence
	public function actionIndex() {
		$Intuit = $this->module->getIntuit();
		if ( $Intuit->handleAuthorizationRespone() ) {
			$this->redirect(array('/qbo/oauth/success'));
		} else {
			$this->redirect(array('/qbo/oauth/error'));
		}
		$this->disableCwebLogOutput();
    	Yii::app()->end();			
	}
	// attempt to grant proper auth tokens
	public function actionGrant() {
		$Intuit = $this->module->getIntuit();
		// Try to handle the OAuth request 
		if( $Intuit->authorizeConnection() ) {
			; // The user has been connected, and will be redirected to /qbo/oauth/success url automatically if all goes well (see actionIndex() )
		} else {
			// If this happens, something went wrong with the OAuth handshake
			throw new CHttpException(502,'Unable to process the authorization request. Review logs.');
		}
		$this->disableCwebLogOutput();
    	Yii::app()->end();		
	}
	// tell me what the current connection status is
	public function actionStatus() {
		$Intuit = $this->module->getIntuit();
		
		if ( $Intuit->isAuthenticated() ) {
			$msg = "CONNECTED??";
			$data = $Intuit->getUserDetails();
		} else {
			$msg = "NOT CONNECTED!";
			$data = array();			
		}
		echo json_encode( array( "msg" => $msg, "data" => $data ));
		Yii::app()->end();		
	}
	/**
	 * provide a pathway for single window oauth connection workflow
	 * it takes the intuit app js function to simulate the redirect in active window
	 * 
	 * @return [type] [description]
	 */
	public function actionConnect() {
		// subtle difference getting object because we do not want a connection verfication
		$Intuit = $this->module->getIntuit();

		if ( $Intuit->isAuthenticated() ) {
			Yii::app()->user->setFlash('notice', "Disconnect existing Oauth-Connection first." );
			$this->redirect(array('/qbo/connection'));		
		} else {
			// without this we will not see the external intuit js loaded
			$this->appendWidgetScripts();
			// this sets up the redirect
			Yii::app()->clientScript->registerScript('quickbooks_direct_connect', QboIntuit::getDirectConnectScript() );

			$this->render("connect");
		}

	}
	// remove the access token from our storage
	public function actionDisconnect() {
		$Intuit = $this->getIntuitObject();
		
		if ( $Intuit->isAlive() ) {
			if ( $Intuit->disconnect() ) {
				$this->render("disconnect");
				$this->disableCwebLogOutput();
				Yii::app()->end();
				return;
			} else {
				Yii::app()->user->setFlash('error', $Intuit->lastError );
			}
		} else {
			Yii::app()->user->setFlash('notice', "No disconnect possible without an active connection." );	
		}
		$this->redirect(array('/qbo/'));
		
		$this->disableCwebLogOutput();
		Yii::app()->end();
	}
	// This is the URL to forward the user to after they have connected to IPP/IDS via OAuth
	public function actionSuccess()
	{
		$this->render("success");

		$this->disableCwebLogOutput();
    	Yii::app()->end();			
	}
	// as per other modules this is available, although intuit does not seem to care about the blue menu anymore 
	public function actionMenu()
	{
		$Intuit = $this->module->getIntuit();
		$html = $Intuit->getAppMenu();

		echo $html;
		// we don't want the extra output of the framework
		$this->disableCwebLogOutput();
    	Yii::app()->end();
	}
	public function actionReconnect() {
		$Intuit = $this->module->getIntuit();
		
		if ( $Intuit->isAlive() ) {
			if ( $Intuit->reconnect() ) {
				Yii::app()->user->setFlash('success', "Renewed access token credentials.");				
			} else {
				if ( $Intuit->lastError ) {
					Yii::app()->user->setFlash('error', $Intuit->lastError );
				} else {
					Yii::app()->user->setFlash('error', "Unable to reconnect.");
				}
			}
		}
		$this->redirect(array('/qbo/'));
		Yii::app()->end();

	}
	public function actionError() {
		$this->render("error");

		$this->disableCwebLogOutput();
    	Yii::app()->end();			
	}

	private function setAppCredentials( $app_token, $consumer_key, $consumer_secret )
	{
		// try loading previous credentials
		$storage = QboToken::model()->findByAttributes( array("app_token" => $app_token, "source" => "USER") );
		// get a clean one if needed
		if ( is_null( $storage ) ) 
			$storage = new QboToken();
		// set the values
		$storage->setTokenData($app_token, $consumer_key, $consumer_secret, NULL, "USER");
		// store it all
		if ( $storage->validate() && $storage->save() ) {
			return true;
		} 
	}
}