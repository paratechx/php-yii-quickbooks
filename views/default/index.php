<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	  Yii::t('app', 'Quickbooks'),
);
?>

<?php $this->renderPartial('/_ipp_button'); ?>
<?php $this->widget('Flasher'); ?>

<h1> Quickbooks Online - Control Center</h1>
<p> </p>

<?php if (! $this->quickbooks_is_authenticated): ?>
	<?php $this->renderPartial('_connect', array() ); ?>
<?php else: ?>
	<p>
		<ul>
			<li> Data Maps: </li>
			<ul>
				<li><?php echo CHtml::link('Users', $this->createAbsoluteUrl('/qbo/employee/list')); ?></li>
				<li> <?php echo CHtml::link('Projects', $this->createAbsoluteUrl('/qbo/customer/list')); ?></li>
				<li> <?php echo CHtml::link('Tasks', $this->createAbsoluteUrl('/qbo/item/list')); ?></li>
			</ul>
			<li> Payroll </li>
			<ul>
				<li> <?php echo CHtml::link('Submit time activities', $this->createAbsoluteUrl('/qbo/timeactivity/export')); ?></li>
				<li> <?php echo CHtml::link('Show submission history', $this->createAbsoluteUrl('/qbo/timeactivity/list')); ?></li>
			</ul>
			<li> <?php echo $this->renderPartial('/_ipp_logout', array("disconnect_url" => $this->disconnect_url, "link_label" => "Disconnect Quickbooks Account")); ?></li>
		</ul>
	</p>
<?php endif; ?>

