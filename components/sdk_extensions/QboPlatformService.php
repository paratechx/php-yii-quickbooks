<?php
/**
 * Quickbooks SDK PlatformService class is poorly written/buggy, so we're overwriting the pieces here that we use! 
 * 
 * @author OnCallSolutions
 * @copyright OnCallSolutions
 * @package  qbo yii-module
 * 
 */
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Exception\SdkExceptions\InvalidTokenException;
use QuickBooksOnline\API\Exception\SdkExceptions\InvalidParameterException;
use QuickBooksOnline\API\Core\HttpClients\RequestParameters;
use QuickBooksOnline\API\Core\HttpClients\SyncRestHandler;
use QuickBooksOnline\API\Core\Http\Serialization\SerializationFormat;
use QuickBooksOnline\API\Core\Http\Compression\CompressionFormat;
use QuickBooksOnline\API\Core\CoreConstants;
use QuickBooksOnline\API\Core\ServiceContext;

class QboPlatformService extends PlatformService
{
    public $lastError = false;
    public $lastXmlResponse = null;

    /**
     * there were some glitches in the original function, correcting them here
     * and reconfiguring the restRequestHandler
     * 
     * @param QuickBooksOnline\API\Core\ServiceContext defining the connnection details
     */
    public function __construct($serviceContext)
    {
        if (null == $serviceContext) {
            throw new ArgumentNullException('Resources.ServiceContextCannotBeNull');
        }

        if (!is_object($serviceContext)) {
            throw new InvalidParameterException('Wrong arg type passed - is not an object.');
        }

        if (!$serviceContext instanceof ServiceContext) {
            throw new InvalidParameterException('Wrong arg type passed - is not the correct class.');
        }

        $serviceContext->UsePlatformServices();
        $this->serviceContext = $serviceContext;

        // redefining the syncRestHandler object so we can overwrite it and correct the faulty code
        $this->restRequestHandler = new QboSyncRestHandler($serviceContext);

        // Set the serviceContext IPP configuration to what IPP is accepting.
        $this->serviceContext->IppConfiguration->Message->Request->SerializationFormat = SerializationFormat::Xml;
        $this->serviceContext->IppConfiguration->Message->Request->CompressionFormat = CompressionFormat::None;
        $this->serviceContext->IppConfiguration->Message->Response->SerializationFormat = SerializationFormat::Xml;
        $this->serviceContext->IppConfiguration->Message->Response->CompressionFormat = CompressionFormat::None;
    }
    /**
     * Disconnect invalidates the OAuth access token in the request, thereby disconnecting
     * the user from QuickBooks for this app.
     *
     * @return SimpleXMLElement Xml of Disconnect response
     */
    public function Disconnect() 
    {
        Yii::$enableIncludePath = false;

        return parent::Disconnect();

        Yii::$enableIncludePath = true;
    }
    /**
     * Reconnect validates the OAuth access token in the request and returns a new one
     *
     * @return SimpleXMLElement Xml of Reconnect response
     */
    public function Reconnect() 
    {
        Yii::$enableIncludePath = false;

        return parent::Reconnect();

        Yii::$enableIncludePath = true;
    }
    /**
     * Returns user information such as first name, last name, and email address.
     *
     * @return SimpleXMLElement Xml of response
     */
    public function CurrentUser()
    {
        Yii::$enableIncludePath = false;

        return parent::CurrentUser();

        Yii::$enableIncludePath = true;
    }   
    /**
     * in theory we will be able to reuse this for all calls, not just reconnect
     * 
     * @param  a response we received from calling a platformService request
     * @return boolean indicating if the response is valid xml
     */
    public function validateXmlResponse( $responseXml )
    {
        if ( $responseXml instanceOf SimpleXMLElement ) {
            if ($responseXml->ErrorCode != '0') {
                if ($responseXml->ErrorCode  == '270')
                {
                    $message = "OAuth Access Token Rejected!";
                }
                else if($responseXml->ErrorCode  == '212')
                {
                    $message = "Token Refresh Window Out of Bounds! The request is made outside the 30-day window bounds.";
                }
                else if($responseXml->ErrorCode  == '24')
                {
                    $message = "Invalid App Token!";
                } else {
                    $message = "Unknown XML error code: ". $responseXml->ErrorCode;
                }
                $this->lastError = $message;
                return false;
            } 
            return true;
        } else {
            $this->lastError = "XmlResponse Error: unable to parse the response as SimpleXMLElement."; 
            return false;
        }
    }
    /**
     * @param  SimpleXMLElement produced by a platformServiceRequest
     * @return boolean confirming that there are tokens in the response
     */
    public function hasXmlTokens( $responseXml ) 
    {
        return ( $responseXml instanceOf SimpleXMLElement &&
                 isset( $responseXml->OAuthToken ) && 
                 isset( $responseXml->OAuthTokenSecret ) );
    }
    /**
     * @param  string $responseXml the data returned by quickbooks api
     * @return boolean indicating if we can parse user details from the api response
     */
    public function hasXmlUserData( $responseXml )
    {
         return ( $responseXml instanceOf SimpleXMLElement &&
                 isset( $responseXml->User ) );   
    }
}