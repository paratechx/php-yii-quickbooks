<?php
/**
 * handles all logic related to the mapping between
 * QUICKBOOKS - employee and HAWKI - users entities
 * 
 */
class ItemController extends DefaultEntityController
{
    protected $model;
    protected $local_entity_name        = "task";
    protected $remote_entity_name       = "item";

    protected function processLocalEntityCreation( $newTask ) 
    {
        $loggedInUser = Yii::app()->user;
        // TODO validate that the user is logged in? Controller should already have caught it!
        if ( $newTask->name ) {
            $newTask->company_id      = $loggedInUser->companyId;
            $newTask->create_user_id  = $loggedInUser->id;
            $newTask->update_user_id  = $loggedInUser->id;
        } else {
            $newTask->addError("name", "Remote Task must have an valid name field set. " );
        }
        return $newTask;
    }

    protected function actionCreateRequiredRemoteAttributes( $local_entity_id ) {
        $request = Yii::app()->getRequest();
        $model = $this->getLocalEntityModel()->findByPk( $local_entity_id );

        if ( $request->isPostRequest ) {    // analyze the response and provide result to our caller
            $modelData = $request->getPost( get_class($model) );
            $action = strtolower( $request->getPost("postaction") );
            $expense_account_id = intval( $request->getPost("expense_account_id") );
            $income_account_id = intval( $request->getPost("income_account_id") );

            // validate
            if ( $action == "create" &&
                 $expense_account_id > 0 &&
                 $income_account_id > 0 ) {
                // pass back to create parent
                return array( "expense_account_id" => $expense_account_id, "income_account_id" => $income_account_id, "rendered" => false );
            } else {
                // TODO -- call addError and tell parent what went wrong
                return array( "rendered" => false );
            }
        } else {  // show the user a selection of required attributes that need to be set before we can proceed
            $dataService = $this->getDataService();
            $expenseAccounts = $dataService->getExpenseAccounts();
            $incomeAccounts = $dataService->getIncomeAccounts();

            if ( ! count( $expenseAccounts ) ) {
                // TODO -- throw errors
            }
            if( ! count( $incomeAccounts ) ) {
                // TODO -- throw errors
            }

            if ( ! is_null( $model ) ) {
                // TODO - figure out the defaultSelector values to make it preselect accounts
                $this->render("create_attributes", array( "model"                   => $model,
                                                          "cancelRoute"             => $this->getListRoute(),
                                                          "expenseAccounts"         => $expenseAccounts, 
                                                          "defaultExpenseAccount"   => "68",    // the select-function compares to a string of the submitted value
                                                          "incomeAccounts"          => $incomeAccounts,
                                                          "defaultIncomeAccount"    => "1",     // not the actual name-label of the value
                                                        ));
            } else {
                // throw invalid local entity error
            }
            return array("rendered" => true );
        }
    
    }    
}