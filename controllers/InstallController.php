<?php
/**
* Rights installation controller class file.
*
* @author OncallSofware
* @copyright OncallSoftware
* @since 
*/
class InstallController extends Controller
{
	/**
	* @property RAuthorizer
	*/
	private $_authorizer;
	/**
	* @property RInstaller
	*/
	private $_installer;
	private $_accessControlRules;

	/**
	* Initializes the controller.
	*/
	public function init() {

		if( $this->module->install!==true )
			$this->redirect(Yii::app()->homeUrl);

		$this->_authorizer = $this->module->getAuthorizer();
		$this->_installer = $this->module->getInstaller();
		$this->layout = $this->module->layout;
		$this->defaultAction = 'start';
	}
	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
		  'accessControl', // perform access control
		);
	}
	/**
	* limit this controller to superusers!
	* @return array access control rules
	*/
	public function accessRules() {
		return array(
			array('allow', // Allow superusers to install qbo module
				'users'		=> $this->_authorizer->getSuperusers(),				
			),
			array('deny', // Deny all users
				'users'=>array('*'),
				'message'	=> 'Quickbooks Online module not installed. Superuser privilages are required to install Quickbooks module.'
			),
		);
	}
	/**
	* Installs the module.
	*/
	public function actionRun()	{
		// Make sure that the module is not already installed.
		if( isset($_GET['confirm'])===true || $this->_installer->installed===false )
		{
			// Run the installer and check for an error.
			if( $this->_installer->run()===QboInstaller::ERROR_NONE )
			{
				Yii::app()->user->setFlash('success', "Database tables setup complete!");
				$this->redirect(array('install/ready'));
			}
			Yii::app()->user->setFlash('error', "Unable to create database schema. Please review logs.");
            // Redirect to the error page.
			$this->redirect(array('install/error'));
		}
		// Module is already istalled.
		else
		{
			$this->redirect(array('install/confirm'));
		}
	}

	/**
	* Displays the install ready page.
	*/
	public function actionReady() {
		$this->render('ready');
	}

    /**
	* Displays the install error.
	*/
	public function actionError() {
		$this->render('error');
	}

    /**
	* Displays the confirm ready page.
	*/
	public function actionConfirm() {
		$this->render('confirm');
	}
	/**
	* The entry point for the install process
	*/ 
	public function actionStart() {
		// if it is installed it will notify user
		if ( $this->_installer->getInstalled() ) {
			$this->render("confirm");
		} else {	
			$this->render("index");
		}
	}
}
