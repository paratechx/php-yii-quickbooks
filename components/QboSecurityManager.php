<?php 
/**
 *
 * Some help to fulfill the security requirements from:
 * 
   https://developer.intuit.com/docs/00_quickbooks_online/4_go_live/30_publish_to_app_store/30_security_requirements

OAuth token management
Verify that your app meets these requirements for OAuth token management.

Intuit OAuth tokens or customer-identifying information must not be exposed within your app or shared with other parties.
Token management once a user completes the OAuth authorization workflow:
OAuth 1.0a
[ ok ] - all stored in qbo_token table  -- Encrypt and store the consumer key, consumer secret, access token, access token secret, and realmId in persistent memory.
[ ok ] - yii utilizes rijndael-128      -- Encrypt the Intuit access token with a symmetric algorithm (3DES or AES). AES is preferred.
[ ok ] - using yii key                  -- Store your AES key in your app, in a separate configuration file.

OAuth 2.0
Encrypt and store the refresh token and realmId in persistent memory.
Encrypt the refresh token with a symmetric algorithm (3DES or AES). AES is preferred.
Store your AES key in your app, in a separate configuration file.
In addition to the above requirements, refer to these best practices for handling OAuth 1.0a tokens or OAuth 2.0 tokens within your app.

----
BEYOND PHP7.0 this will become a problem!
https://github.com/yiisoft/yii/issues/4119

as of php 7.1 the mcrypt_encode function is deprecated - will raise flags
wrapper functions, to be replaced when client upgrades php > 7.0, with openssl_...

--- 
per http://www.yiiframework.com/forum/index.php/topic/49296-using-csecuritymanager-to-encryptdecrypt-data/
we need the utf_8 encode/decode as well...


***/



class QboSecurityManager extends CSecurityManager {
  /**
   * Encrypts data.
   * @param string $data data to be encrypted.
   * @param string $key the decryption key. This defaults to null, meaning using {@link getEncryptionKey EncryptionKey}.
   * @return string the encrypted data
   * @throws CException if PHP Mcrypt extension is not loaded or key is invalid
   */
  public function encrypt($data,$key=null)
  {
    if ( ! empty( $data ) ) {
      $encrypted = parent::encrypt( $data, $key );
      return utf8_encode( $encrypted );
    } else {
      return $data;
    }  
  }

  /**
   * Decrypts data
   * @param string $data data to be decrypted.
   * @param string $key the decryption key. This defaults to null, meaning using {@link getEncryptionKey EncryptionKey}.
   * @return string the decrypted data
   * @throws CException if PHP Mcrypt extension is not loaded or key is invalid
   */
  public function decrypt($data,$key=null)
  {
    if ( ! empty( $data ) ) {
      $data = utf8_decode( $data );
      $decrypted = parent::decrypt( $data, $key );
      return $decrypted;
    } else {
      return $data;
    }  
  }    
} 