<div id="installer" class="ready">

	<h2><?php echo Yii::t('install', 'Congratulations!'); ?></h2>

	<p class="green-text">
		<?php echo Yii::t('install', 'Quickbooks Online (Qbo) has been installed succesfully.'); ?>
	</p>

	<p>
		<?php echo Yii::t('install', 'You can start by linking this user to your online quickbooks account.') ;?>
		<?php echo CHtml::link(Yii::t('install', 'here'), array('/qbo')); ?>.
	</p>

</div>